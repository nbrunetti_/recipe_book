-- phpMyAdmin SQL Dump
-- version 4.4.15.8
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Creato il: Gen 16, 2017 alle 00:21
-- Versione del server: 5.6.31
-- Versione PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recipe_book`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `code3l` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `code2l` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `name_official` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag_32` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag_128` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `zoom` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `country`
--

INSERT INTO `country` (`id`, `enabled`, `code3l`, `code2l`, `name`, `name_official`, `flag_32`, `flag_128`, `latitude`, `longitude`, `zoom`) VALUES
(1, 1, 'AFG', 'AF', 'Afghanistan', 'The Islamic Republic of Afghanistan', 'AF-32.png', 'AF-128.png', '33.98299275', '66.39159363', 6),
(2, 1, 'ALB', 'AL', 'Albania', 'the Republic of Albania', 'AL-32.png', 'AL-128.png', '41.00017358', '19.87170014', 7),
(3, 1, 'DZA', 'DZ', 'Algeria', 'the People''s Democratic Republic of Algeria', 'DZ-32.png', 'DZ-128.png', '27.89861690', '3.19771194', 5),
(4, 1, 'AND', 'AD', 'Andorra', 'the Principality of Andorra', 'AD-32.png', 'AD-128.png', '42.54057088', '1.55201340', 11),
(5, 1, 'AGO', 'AO', 'Angola', 'the Republic of Angola', 'AO-32.png', 'AO-128.png', '-12.16469683', '16.70933622', 6),
(6, 1, 'ATG', 'AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'AG-32.png', 'AG-128.png', '17.48060423', '-61.42014426', 9),
(7, 1, 'ARG', 'AR', 'Argentina', 'the Argentine Republic', 'AR-32.png', 'AR-128.png', '-38.01529308', '-64.97897469', 4),
(8, 1, 'ARM', 'AM', 'Armenia', 'the Republic of Armenia', 'AM-32.png', 'AM-128.png', '40.13475528', '45.01072318', 7),
(9, 1, 'AUS', 'AU', 'Australia', 'Australia', 'AU-32.png', 'AU-128.png', '-26.29594646', '133.55540944', 4),
(10, 1, 'AUT', 'AT', 'Austria', 'the Republic of Austria', 'AT-32.png', 'AT-128.png', '47.63125476', '13.18776731', 7),
(11, 1, 'AZE', 'AZ', 'Azerbaijan', 'the Republic of Azerbaijan', 'AZ-32.png', 'AZ-128.png', '40.35321757', '47.46706372', 7),
(12, 1, 'BHS', 'BS', 'Bahamas', 'the Commonwealth of the Bahamas', 'BS-32.png', 'BS-128.png', '24.45991732', '-77.68192453', 7),
(13, 1, 'BHR', 'BH', 'Bahrain', 'the Kingdom of Bahrain', 'BH-32.png', 'BH-128.png', '25.90740996', '50.65932354', 9),
(14, 1, 'BGD', 'BD', 'Bangladesh', 'the People''s Republic of Bangladesh', 'BD-32.png', 'BD-128.png', '24.08273251', '90.49915527', 7),
(15, 1, 'BRB', 'BB', 'Barbados', 'Barbados', 'BB-32.png', 'BB-128.png', '13.19383077', '-59.54319600', 11),
(16, 1, 'BLR', 'BY', 'Belarus', 'the Republic of Belarus', 'BY-32.png', 'BY-128.png', '53.58628747', '27.95338900', 6),
(17, 1, 'BEL', 'BE', 'Belgium', 'the Kingdom of Belgium', 'BE-32.png', 'BE-128.png', '50.49593874', '4.46993600', 8),
(18, 1, 'BLZ', 'BZ', 'Belize', 'Belize', 'BZ-32.png', 'BZ-128.png', '17.21153631', '-88.01424956', 8),
(19, 1, 'BEN', 'BJ', 'Benin', 'the Republic of Benin', 'BJ-32.png', 'BJ-128.png', '9.37180859', '2.29386134', 7),
(20, 1, 'BTN', 'BT', 'Bhutan', 'the Kingdom of Bhutan', 'BT-32.png', 'BT-128.png', '27.50752756', '90.43360300', 8),
(21, 1, 'BIH', 'BA', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BA-32.png', 'BA-128.png', '44.00040856', '17.81640910', 7),
(22, 1, 'BWA', 'BW', 'Botswana', 'the Republic of Botswana', 'BW-32.png', 'BW-128.png', '-22.18279485', '24.22344422', 6),
(23, 1, 'BRA', 'BR', 'Brazil', 'the Federative Republic of Brazil', 'BR-32.png', 'BR-128.png', '-11.80965046', '-53.33152600', 4),
(24, 1, 'BRN', 'BN', 'Brunei Darussalam', 'Brunei Darussalam', 'BN-32.png', 'BN-128.png', '4.54189364', '114.60132823', 9),
(25, 1, 'BGR', 'BG', 'Bulgaria', 'the Republic of Bulgaria', 'BG-32.png', 'BG-128.png', '42.70160678', '25.48583200', 7),
(26, 1, 'BFA', 'BF', 'Burkina Faso', 'Burkina Faso', 'BF-32.png', 'BF-128.png', '12.22492458', '-1.56159100', 7),
(27, 1, 'BDI', 'BI', 'Burundi', 'the Republic of Burundi', 'BI-32.png', 'BI-128.png', '-3.40499707', '29.88592902', 8),
(28, 1, 'KHM', 'KH', 'Cambodia', 'the Kingdom of Cambodia', 'KH-32.png', 'KH-128.png', '12.83288883', '104.84814273', 7),
(29, 1, 'CMR', 'CM', 'Cameroon', 'the Republic of Cameroon', 'CM-32.png', 'CM-128.png', '7.38622543', '12.72825915', 6),
(30, 1, 'CAN', 'CA', 'Canada', 'Canada', 'CA-32.png', 'CA-128.png', '60.36196817', '-106.69833150', 4),
(31, 1, 'CPV', 'CV', 'Cabo Verde', 'Republic of Cabo Verde', 'CV-32.png', 'CV-128.png', '15.11988711', '-23.60517010', 10),
(32, 1, 'CAF', 'CF', 'Central African Republic', 'the Central African Republic', 'CF-32.png', 'CF-128.png', '6.82541830', '20.64281514', 6),
(33, 1, 'TCD', 'TD', 'Chad', 'the Republic of Chad', 'TD-32.png', 'TD-128.png', '14.80342407', '18.78714064', 5),
(34, 1, 'CHL', 'CL', 'Chile', 'the Republic of Chile', 'CL-32.png', 'CL-128.png', '-38.01760790', '-71.40014474', 4),
(35, 1, 'CHN', 'CN', 'China', 'the People''s Republic of China', 'CN-32.png', 'CN-128.png', '36.71457440', '103.55819197', 4),
(36, 1, 'COL', 'CO', 'Colombia', 'the Republic of Colombia', 'CO-32.png', 'CO-128.png', '3.68182320', '-73.53927436', 5),
(37, 1, 'COM', 'KM', 'Comoros', 'the Union of the Comoros', 'KM-32.png', 'KM-128.png', '-11.64529989', '43.33330200', 10),
(38, 1, 'COG', 'CG', 'Congo', 'the Republic of the Congo', 'CG-32.png', 'CG-128.png', '-0.68967806', '15.69033190', 6),
(39, 1, 'CRI', 'CR', 'Costa Rica', 'the Republic of Costa Rica', 'CR-32.png', 'CR-128.png', '9.98427463', '-84.09949534', 8),
(40, 1, 'HRV', 'HR', 'Croatia', 'the Republic of Croatia', 'HR-32.png', 'HR-128.png', '44.81372482', '16.29039507', 7),
(41, 1, 'CUB', 'CU', 'Cuba', 'the Republic of Cuba', 'CU-32.png', 'CU-128.png', '21.54513189', '-79.00064743', 6),
(42, 1, 'CYP', 'CY', 'Cyprus', 'the Republic of Cyprus', 'CY-32.png', 'CY-128.png', '35.12450768', '33.42986100', 9),
(43, 1, 'CZE', 'CZ', 'Czech Republic', 'the Czech Republic', 'CZ-32.png', 'CZ-128.png', '49.76026136', '15.53888197', 7),
(44, 1, 'CIV', 'CI', 'Côte d''Ivoire', 'the Republic of Côte d''Ivoire', 'CI-32.png', 'CI-128.png', '7.59684148', '-5.49214636', 7),
(45, 1, 'DNK', 'DK', 'Denmark', 'the Kingdom of Denmark', 'DK-32.png', 'DK-128.png', '54.71794021', '9.41938953', 6),
(46, 1, 'DJI', 'DJ', 'Djibouti', 'the Republic of Djibouti', 'DJ-32.png', 'DJ-128.png', '11.75959257', '42.65344839', 8),
(47, 1, 'DMA', 'DM', 'Dominica', 'the Commonwealth of Dominica', 'DM-32.png', 'DM-128.png', '15.41473963', '-61.37097400', 10),
(48, 1, 'DOM', 'DO', 'Dominican Republic', 'the Dominican Republic', 'DO-32.png', 'DO-128.png', '18.73076761', '-70.16264900', 8),
(49, 1, 'ECU', 'EC', 'Ecuador', 'the Republic of Ecuador', 'EC-32.png', 'EC-128.png', '-1.22919037', '-78.55693916', 6),
(50, 1, 'EGY', 'EG', 'Egypt', 'the Arab Republic of Egypt', 'EG-32.png', 'EG-128.png', '26.71650873', '30.80250000', 6),
(51, 1, 'SLV', 'SV', 'El Salvador', 'the Republic of El Salvador', 'SV-32.png', 'SV-128.png', '13.79043561', '-88.89652800', 8),
(52, 1, 'GNQ', 'GQ', 'Equatorial Guinea', 'the Republic of Equatorial Guinea', 'GQ-32.png', 'GQ-128.png', '1.65068442', '10.26789700', 9),
(53, 1, 'ERI', 'ER', 'Eritrea', 'the State of Eritrea', 'ER-32.png', 'ER-128.png', '15.21227764', '39.61204792', 7),
(54, 1, 'EST', 'EE', 'Estonia', 'the Republic of Estonia', 'EE-32.png', 'EE-128.png', '58.74041141', '25.38165099', 7),
(55, 1, 'ETH', 'ET', 'Ethiopia', 'the Federal Democratic Republic of Ethiopia', 'ET-32.png', 'ET-128.png', '9.10727589', '39.84148164', 6),
(56, 1, 'FJI', 'FJ', 'Fiji', 'the Republic of Fiji', 'FJ-32.png', 'FJ-128.png', '-17.71219757', '178.06503600', 9),
(57, 1, 'FIN', 'FI', 'Finland', 'the Republic of Finland', 'FI-32.png', 'FI-128.png', '64.69610892', '26.36339137', 5),
(58, 1, 'FRA', 'FR', 'France', 'the French Republic', 'FR-32.png', 'FR-128.png', '46.48372145', '2.60926281', 6),
(59, 1, 'GAB', 'GA', 'Gabon', 'the Gabonese Republic', 'GA-32.png', 'GA-128.png', '-0.43426435', '11.43916591', 7),
(60, 1, 'GMB', 'GM', 'Gambia', 'Islamic Republic of the Gambia', 'GM-32.png', 'GM-128.png', '13.15921146', '-15.35956748', 8),
(61, 1, 'GEO', 'GE', 'Georgia', 'Georgia', 'GE-32.png', 'GE-128.png', '41.82754301', '44.17329916', 7),
(62, 1, 'DEU', 'DE', 'Germany', 'the Federal Republic of Germany', 'DE-32.png', 'DE-128.png', '50.82871201', '10.97887975', 6),
(63, 1, 'GHA', 'GH', 'Ghana', 'the Republic of Ghana', 'GH-32.png', 'GH-128.png', '7.69154199', '-1.29234904', 7),
(64, 1, 'GRC', 'GR', 'Greece', 'the Hellenic Republic', 'GR-32.png', 'GR-128.png', '38.52254746', '24.53794505', 6),
(65, 1, 'GRD', 'GD', 'Grenada', 'Grenada', 'GD-32.png', 'GD-128.png', '12.11644807', '-61.67899400', 11),
(66, 1, 'GTM', 'GT', 'Guatemala', 'the Republic of Guatemala', 'GT-32.png', 'GT-128.png', '15.72598421', '-89.96707712', 7),
(67, 1, 'GIN', 'GN', 'Guinea', 'the Republic of Guinea', 'GN-32.png', 'GN-128.png', '9.94301472', '-11.31711839', 7),
(68, 1, 'GNB', 'GW', 'Guinea-Bissau', 'the Republic of Guinea-Bissau', 'GW-32.png', 'GW-128.png', '11.80050682', '-15.18040700', 8),
(69, 1, 'GUY', 'GY', 'Guyana', 'the Republic of Guyana', 'GY-32.png', 'GY-128.png', '4.47957059', '-58.72692293', 6),
(70, 1, 'HTI', 'HT', 'Haiti', 'the Republic of Haiti', 'HT-32.png', 'HT-128.png', '19.07430861', '-72.79607526', 8),
(71, 1, 'HND', 'HN', 'Honduras', 'the Republic of Honduras', 'HN-32.png', 'HN-128.png', '14.64994423', '-87.01643713', 7),
(72, 1, 'HUN', 'HU', 'Hungary', 'Hungary', 'HU-32.png', 'HU-128.png', '46.97670384', '19.35499657', 7),
(73, 1, 'ISL', 'IS', 'Iceland', 'the Republic of Iceland', 'IS-32.png', 'IS-128.png', '64.99294495', '-18.57038755', 6),
(74, 1, 'IND', 'IN', 'India', 'the Republic of India', 'IN-32.png', 'IN-128.png', '20.46549519', '78.50146222', 4),
(75, 1, 'IDN', 'ID', 'Indonesia', 'the Republic of Indonesia', 'ID-32.png', 'ID-128.png', '-2.46229680', '121.18329789', 4),
(76, 1, 'IRQ', 'IQ', 'Iraq', 'the Republic of Iraq', 'IQ-32.png', 'IQ-128.png', '32.90170182', '43.19590056', 6),
(77, 1, 'IRL', 'IE', 'Ireland', 'Ireland', 'IE-32.png', 'IE-128.png', '53.10101628', '-8.21092302', 6),
(78, 1, 'ISR', 'IL', 'Israel', 'the State of Israel', 'IL-32.png', 'IL-128.png', '30.85883075', '34.91753797', 7),
(79, 1, 'ITA', 'IT', 'Italy', 'the Republic of Italy', 'IT-32.png', 'IT-128.png', '41.77810840', '12.67725128', 5),
(80, 1, 'JAM', 'JM', 'Jamaica', 'Jamaica', 'JM-32.png', 'JM-128.png', '18.10838487', '-77.29750600', 9),
(81, 1, 'JPN', 'JP', 'Japan', 'Japan', 'JP-32.png', 'JP-128.png', '37.51848822', '137.67066061', 5),
(82, 1, 'JOR', 'JO', 'Jordan', 'the Hashemite Kingdom of Jordan', 'JO-32.png', 'JO-128.png', '31.31616588', '36.37575510', 7),
(83, 1, 'KAZ', 'KZ', 'Kazakhstan', 'the Republic of Kazakhstan', 'KZ-32.png', 'KZ-128.png', '45.38592596', '68.81334444', 4),
(84, 1, 'KEN', 'KE', 'Kenya', 'the Republic of Kenya', 'KE-32.png', 'KE-128.png', '0.19582452', '37.97212297', 6),
(85, 1, 'KIR', 'KI', 'Kiribati', 'the Republic of Kiribati', 'KI-32.png', 'KI-128.png', '1.87085244', '-157.36259310', 10),
(86, 1, 'KWT', 'KW', 'Kuwait', 'the State of Kuwait', 'KW-32.png', 'KW-128.png', '29.43253341', '47.71798405', 8),
(87, 1, 'KGZ', 'KG', 'Kyrgyzstan', 'the Kyrgyz Republic', 'KG-32.png', 'KG-128.png', '41.11509878', '74.25524574', 6),
(88, 1, 'LAO', 'LA', 'Lao People''s Democratic Republic', 'the Lao People''s Democratic Republic', 'LA-32.png', 'LA-128.png', '17.76075593', '103.61611347', 6),
(89, 1, 'LVA', 'LV', 'Latvia', 'the Republic of Latvia', 'LV-32.png', 'LV-128.png', '56.86697515', '24.54826936', 7),
(90, 1, 'LBN', 'LB', 'Lebanon', 'the Lebanese Republic', 'LB-32.png', 'LB-128.png', '34.08249284', '35.66454309', 8),
(91, 1, 'LSO', 'LS', 'Lesotho', 'the Kingdom of Lesotho', 'LS-32.png', 'LS-128.png', '-29.60303205', '28.23361200', 8),
(92, 1, 'LBR', 'LR', 'Liberia', 'the Republic of Liberia', 'LR-32.png', 'LR-128.png', '6.44154681', '-9.39103485', 7),
(93, 1, 'LBY', 'LY', 'Libya', 'Libya', 'LY-32.png', 'LY-128.png', '27.06902914', '18.19513987', 5),
(94, 1, 'LIE', 'LI', 'Liechtenstein', 'the Principality of Liechtenstein', 'LI-32.png', 'LI-128.png', '47.16587383', '9.55537700', 11),
(95, 1, 'LTU', 'LT', 'Lithuania', 'the Republic of Lithuania', 'LT-32.png', 'LT-128.png', '55.25095948', '23.80987587', 7),
(96, 1, 'LUX', 'LU', 'Luxembourg', 'the Grand Duchy of Luxembourg', 'LU-32.png', 'LU-128.png', '49.81327712', '6.12958700', 9),
(97, 1, 'MDG', 'MG', 'Madagascar', 'the Republic of Madagascar', 'MG-32.png', 'MG-128.png', '-19.79858543', '46.97898228', 5),
(98, 1, 'MWI', 'MW', 'Malawi', 'the Republic of Malawi', 'MW-32.png', 'MW-128.png', '-12.48684092', '34.14223524', 6),
(99, 1, 'MYS', 'MY', 'Malaysia', 'Malaysia', 'MY-32.png', 'MY-128.png', '4.97345793', '106.54609050', 5),
(100, 1, 'MDV', 'MV', 'Maldives', 'the Republic of Maldives', 'MV-32.png', 'MV-128.png', '-0.64224221', '73.13373313', 12),
(101, 1, 'MLI', 'ML', 'Mali', 'the Republic of Mali', 'ML-32.png', 'ML-128.png', '17.69385811', '-1.96368730', 5),
(102, 1, 'MLT', 'MT', 'Malta', 'the Republic of Malta', 'MT-32.png', 'MT-128.png', '35.89706403', '14.43687877', 11),
(103, 1, 'MHL', 'MH', 'Marshall Islands', 'the Republic of the Marshall Islands', 'MH-32.png', 'MH-128.png', '7.30130732', '168.75512619', 10),
(104, 1, 'MRT', 'MR', 'Mauritania', 'the Islamic Republic of Mauritania', 'MR-32.png', 'MR-128.png', '20.28331239', '-10.21573334', 5),
(105, 1, 'MUS', 'MU', 'Mauritius', 'the Republic of Mauritius', 'MU-32.png', 'MU-128.png', '-20.28368188', '57.56588291', 10),
(106, 1, 'MEX', 'MX', 'Mexico', 'the United Mexican States', 'MX-32.png', 'MX-128.png', '22.92036676', '-102.33305344', 5),
(107, 1, 'MCO', 'MC', 'Monaco', 'the Principality of Monaco', 'MC-32.png', 'MC-128.png', '43.70463620', '6.75444978', 9),
(108, 1, 'MNG', 'MN', 'Mongolia', 'Mongolia', 'MN-32.png', 'MN-128.png', '46.80556270', '104.30808978', 5),
(109, 1, 'MNE', 'ME', 'Montenegro', 'Montenegro', 'ME-32.png', 'ME-128.png', '42.71699590', '19.09699321', 8),
(110, 1, 'MAR', 'MA', 'Morocco', 'the Kingdom of Morocco', 'MA-32.png', 'MA-128.png', '31.95441758', '-7.26839325', 6),
(111, 1, 'MOZ', 'MZ', 'Mozambique', 'the Republic of Mozambique', 'MZ-32.png', 'MZ-128.png', '-19.07617816', '33.81570282', 5),
(112, 1, 'MMR', 'MM', 'Myanmar', 'the Republic of the Union of Myanmar', 'MM-32.png', 'MM-128.png', '19.20985380', '96.54949272', 5),
(113, 1, 'NAM', 'NA', 'Namibia', 'the Republic of Namibia', 'NA-32.png', 'NA-128.png', '-22.70965620', '16.72161918', 6),
(114, 1, 'NRU', 'NR', 'Nauru', 'the Republic of Nauru', 'NR-32.png', 'NR-128.png', '-0.52586763', '166.93270463', 13),
(115, 1, 'NPL', 'NP', 'Nepal', 'the Federal Democratic Republic of Nepal', 'NP-32.png', 'NP-128.png', '28.28430770', '83.98119373', 7),
(116, 1, 'NLD', 'NL', 'Netherlands', 'the Kingdom of the Netherlands', 'NL-32.png', 'NL-128.png', '52.33939951', '4.98914998', 7),
(117, 1, 'NZL', 'NZ', 'New Zealand', 'New Zealand', 'NZ-32.png', 'NZ-128.png', '-40.95025298', '171.76586181', 5),
(118, 1, 'NIC', 'NI', 'Nicaragua', 'the Republic of Nicaragua', 'NI-32.png', 'NI-128.png', '12.91806226', '-84.82270352', 7),
(119, 1, 'NER', 'NE', 'Niger', 'the Republic of the Niger', 'NE-32.png', 'NE-128.png', '17.23446679', '8.23547860', 6),
(120, 1, 'NGA', 'NG', 'Nigeria', 'the Federal Republic of Nigeria', 'NG-32.png', 'NG-128.png', '9.02165273', '7.82933373', 6),
(121, 1, 'NOR', 'NO', 'Norway', 'the Kingdom of Norway', 'NO-32.png', 'NO-128.png', '65.04680297', '13.50069228', 4),
(122, 1, 'OMN', 'OM', 'Oman', 'the Sultanate of Oman', 'OM-32.png', 'OM-128.png', '20.69906846', '56.69230596', 6),
(123, 1, 'PAK', 'PK', 'Pakistan', 'the Islamic Republic of Pakistan', 'PK-32.png', 'PK-128.png', '29.90335974', '70.34487986', 5),
(124, 1, 'PLW', 'PW', 'Palau', 'the Republic of Palau', 'PW-32.png', 'PW-128.png', '7.49856307', '134.57291496', 10),
(125, 1, 'PAN', 'PA', 'Panama', 'the Republic of Panama', 'PA-32.png', 'PA-128.png', '8.52135102', '-80.04603702', 7),
(126, 1, 'PNG', 'PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PG-32.png', 'PG-128.png', '-6.62414046', '144.44993477', 7),
(127, 1, 'PRY', 'PY', 'Paraguay', 'the Republic of Paraguay', 'PY-32.png', 'PY-128.png', '-23.38564782', '-58.29551057', 6),
(128, 1, 'PER', 'PE', 'Peru', 'the Republic of Peru', 'PE-32.png', 'PE-128.png', '-8.50205247', '-76.15772412', 5),
(129, 1, 'PHL', 'PH', 'Philippines', 'the Republic of the Philippines', 'PH-32.png', 'PH-128.png', '12.82361200', '121.77401700', 6),
(130, 1, 'POL', 'PL', 'Poland', 'the Republic of Poland', 'PL-32.png', 'PL-128.png', '52.10117636', '19.33190957', 6),
(131, 1, 'PRT', 'PT', 'Portugal', 'the Portuguese Republic', 'PT-32.png', 'PT-128.png', '39.44879136', '-8.03768042', 6),
(132, 1, 'QAT', 'QA', 'Qatar', 'the State of Qatar', 'QA-32.png', 'QA-128.png', '25.24551555', '51.24431480', 8),
(133, 1, 'ROU', 'RO', 'Romania', 'Romania', 'RO-32.png', 'RO-128.png', '45.56450023', '25.21945155', 6),
(134, 1, 'RUS', 'RU', 'Russian Federation', 'the Russian Federation', 'RU-32.png', 'RU-128.png', '57.96812298', '102.41837137', 3),
(135, 1, 'RWA', 'RW', 'Rwanda', 'the Republic of Rwanda', 'RW-32.png', 'RW-128.png', '-1.98589079', '29.94255855', 8),
(136, 1, 'KNA', 'KN', 'Saint Kitts and Nevis', 'Saint Kitts and Nevis', 'KN-32.png', 'KN-128.png', '17.33453669', '-62.76411725', 12),
(137, 1, 'LCA', 'LC', 'Saint Lucia', 'Saint Lucia', 'LC-32.png', 'LC-128.png', '13.90938495', '-60.97889500', 11),
(138, 1, 'VCT', 'VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VC-32.png', 'VC-128.png', '13.25276143', '-61.19709800', 11),
(139, 1, 'WSM', 'WS', 'Samoa', 'the Independent State of Samoa', 'WS-32.png', 'WS-128.png', '-13.57998954', '-172.45207363', 10),
(140, 1, 'SMR', 'SM', 'San Marino', 'the Republic of San Marino', 'SM-32.png', 'SM-128.png', '43.94223356', '12.45777700', 11),
(141, 1, 'STP', 'ST', 'Sao Tome and Principe', 'the Democratic Republic of Sao Tome and Principe', 'ST-32.png', 'ST-128.png', '0.23381910', '6.59935809', 10),
(142, 1, 'SAU', 'SA', 'Saudi Arabia', 'the Kingdom of Saudi Arabia', 'SA-32.png', 'SA-128.png', '24.16687314', '42.88190638', 5),
(143, 1, 'SEN', 'SN', 'Senegal', 'the Republic of Senegal', 'SN-32.png', 'SN-128.png', '14.43579003', '-14.68306489', 7),
(144, 1, 'SRB', 'RS', 'Serbia', 'the Republic of Serbia', 'RS-32.png', 'RS-128.png', '44.06736041', '20.29725084', 7),
(145, 1, 'SYC', 'SC', 'Seychelles', 'the Republic of Seychelles', 'SC-32.png', 'SC-128.png', '-4.68053204', '55.49061371', 11),
(146, 1, 'SLE', 'SL', 'Sierra Leone', 'the Republic of Sierra Leone', 'SL-32.png', 'SL-128.png', '8.45575589', '-11.93368759', 8),
(147, 1, 'SGP', 'SG', 'Singapore', 'the Republic of Singapore', 'SG-32.png', 'SG-128.png', '1.33873261', '103.83323559', 11),
(148, 1, 'SVK', 'SK', 'Slovakia', 'the Slovak Republic', 'SK-32.png', 'SK-128.png', '48.66923253', '19.75396564', 7),
(149, 1, 'SVN', 'SI', 'Slovenia', 'the Republic of Slovenia', 'SI-32.png', 'SI-128.png', '46.14315048', '14.99546300', 8),
(150, 1, 'SLB', 'SB', 'Solomon Islands', 'Solomon Islands', 'SB-32.png', 'SB-128.png', '-9.64554280', '160.15619400', 10),
(151, 1, 'SOM', 'SO', 'Somalia', 'the Federal Republic of Somalia', 'SO-32.png', 'SO-128.png', '2.87224619', '45.27676444', 7),
(152, 1, 'ZAF', 'ZA', 'South Africa', 'the Republic of South Africa', 'ZA-32.png', 'ZA-128.png', '-27.17706863', '24.50856092', 5),
(153, 1, 'ESP', 'ES', 'Spain', 'the Kingdom of Spain', 'ES-32.png', 'ES-128.png', '39.87299401', '-3.67089492', 6),
(154, 1, 'LKA', 'LK', 'Sri Lanka', 'the Democratic Socialist Republic of Sri Lanka', 'LK-32.png', 'LK-128.png', '7.61264985', '80.83772497', 7),
(155, 1, 'SDN', 'SD', 'Sudan', 'the Republic of the Sudan', 'SD-32.png', 'SD-128.png', '15.96646839', '30.37145459', 5),
(156, 1, 'SUR', 'SR', 'Suriname', 'the Republic of Suriname', 'SR-32.png', 'SR-128.png', '4.26470865', '-55.93988238', 7),
(157, 1, 'SWZ', 'SZ', 'Swaziland', 'the Kingdom of Swaziland', 'SZ-32.png', 'SZ-128.png', '-26.53892570', '31.47960891', 9),
(158, 1, 'SWE', 'SE', 'Sweden', 'the Kingdom of Sweden', 'SE-32.png', 'SE-128.png', '61.42370427', '16.73188991', 4),
(159, 1, 'CHE', 'CH', 'Switzerland', 'the Swiss Confederation', 'CH-32.png', 'CH-128.png', '46.81010721', '8.22751200', 8),
(160, 1, 'SYR', 'SY', 'Syrian Arab Republic', 'the Syrian Arab Republic', 'SY-32.png', 'SY-128.png', '34.71097430', '38.66723516', 6),
(161, 1, 'TJK', 'TJ', 'Tajikistan', 'the Republic of Tajikistan', 'TJ-32.png', 'TJ-128.png', '38.68075124', '71.23215769', 7),
(162, 1, 'THA', 'TH', 'Thailand', 'the Kingdom of Thailand', 'TH-32.png', 'TH-128.png', '14.60009810', '101.38805881', 5),
(163, 1, 'TLS', 'TL', 'Timor-Leste', 'the Democratic Republic of Timor-Leste', 'TL-32.png', 'TL-128.png', '-8.88926365', '125.99671404', 9),
(164, 1, 'TGO', 'TG', 'Togo', 'the Togolese Republic', 'TG-32.png', 'TG-128.png', '8.68089206', '0.86049757', 7),
(165, 1, 'TON', 'TO', 'Tonga', 'the Kingdom of Tonga', 'TO-32.png', 'TO-128.png', '-21.17890075', '-175.19824200', 11),
(166, 1, 'TTO', 'TT', 'Trinidad and Tobago', 'the Republic of Trinidad and Tobago', 'TT-32.png', 'TT-128.png', '10.43241863', '-61.22250300', 10),
(167, 1, 'TUN', 'TN', 'Tunisia', 'the Republic of Tunisia', 'TN-32.png', 'TN-128.png', '33.88431940', '9.71878341', 6),
(168, 1, 'TUR', 'TR', 'Turkey', 'the Republic of Turkey', 'TR-32.png', 'TR-128.png', '38.27069555', '36.28703317', 5),
(169, 1, 'TKM', 'TM', 'Turkmenistan', 'Turkmenistan', 'TM-32.png', 'TM-128.png', '38.94915421', '59.06190323', 6),
(170, 1, 'TUV', 'TV', 'Tuvalu', 'Tuvalu', 'TV-32.png', 'TV-128.png', '-8.45968122', '179.13310944', 12),
(171, 1, 'UGA', 'UG', 'Uganda', 'the Republic of Uganda', 'UG-32.png', 'UG-128.png', '1.54760620', '32.44409759', 7),
(172, 1, 'UKR', 'UA', 'Ukraine', 'Ukraine', 'UA-32.png', 'UA-128.png', '48.89358596', '31.10516920', 6),
(173, 1, 'ARE', 'AE', 'United Arab Emirates', 'the United Arab Emirates', 'AE-32.png', 'AE-128.png', '24.64324405', '53.62261227', 7),
(174, 1, 'URY', 'UY', 'Uruguay', 'the Eastern Republic of Uruguay', 'UY-32.png', 'UY-128.png', '-32.49342987', '-55.76583300', 7),
(175, 1, 'UZB', 'UZ', 'Uzbekistan', 'the Republic of Uzbekistan', 'UZ-32.png', 'UZ-128.png', '41.30829147', '62.62970960', 6),
(176, 1, 'VUT', 'VU', 'Vanuatu', 'the Republic of Vanuatu', 'VU-32.png', 'VU-128.png', '-15.37256614', '166.95916000', 8),
(177, 1, 'VNM', 'VN', 'Viet Nam', 'the Socialist Republic of Viet Nam', 'VN-32.png', 'VN-128.png', '17.19931699', '107.14012804', 5),
(178, 1, 'YEM', 'YE', 'Yemen', 'the Republic of Yemen', 'YE-32.png', 'YE-128.png', '15.60865453', '47.60453676', 6),
(179, 1, 'ZMB', 'ZM', 'Zambia', 'the Republic of Zambia', 'ZM-32.png', 'ZM-128.png', '-13.01812188', '28.33274444', 6),
(180, 1, 'ZWE', 'ZW', 'Zimbabwe', 'the Republic of Zimbabwe', 'ZW-32.png', 'ZW-128.png', '-19.00784952', '30.18758584', 6),
(181, 1, 'COK', 'CK', 'Cook Islands', 'the Cook Islands', 'CK-32.png', 'CK-128.png', '-21.23673066', '-159.77766900', 13),
(182, 1, 'BOL', 'BO', 'Bolivia (Plurinational State of)', 'the Plurinational State of Bolivia', 'BO-32.png', 'BO-128.png', '-16.74518128', '-65.19265691', 6),
(183, 1, 'COD', 'CD', 'Democratic Republic of the Congo', 'the Democratic Republic of the Congo', 'CD-32.png', 'CD-128.png', '-4.05373938', '23.01110741', 5),
(184, 1, 'EUR', 'EU', 'European Union', 'European Union', 'EU-32.png', 'EU-128.png', '48.76380654', '14.26843140', 3),
(185, 1, 'FSM', 'FM', 'Micronesia (Federated States of)', 'the Federated States of Micronesia', 'FM-32.png', 'FM-128.png', '6.88747377', '158.21507170', 12),
(186, 1, 'GBR', 'GB', 'United Kingdom', 'the United Kingdom of Great Britain and Northern Ireland', 'GB-32.png', 'GB-128.png', '53.36540813', '-2.72184767', 6),
(187, 1, 'IRN', 'IR', 'Iran (Islamic Republic of)', 'the Islamic Republic of Iran', 'IR-32.png', 'IR-128.png', '31.40240324', '51.28204814', 5),
(188, 1, 'PRK', 'KP', 'Democratic People''s Republic of Korea', 'the Democratic People''s Republic of Korea', 'KP-32.png', 'KP-128.png', '40.00785500', '127.48812834', 6),
(189, 1, 'KOR', 'KR', 'Republic of Korea', 'the Republic of Korea', 'KR-32.png', 'KR-128.png', '36.56344139', '127.51424646', 7),
(190, 1, 'MDA', 'MD', 'Republic of Moldova', 'the Republic of Moldova', 'MD-32.png', 'MD-128.png', '47.10710437', '28.54018109', 7),
(191, 1, 'MKD', 'MK', 'The former Yugoslav Republic of Macedonia', 'The former Yugoslav Republic of Macedonia', 'MK-32.png', 'MK-128.png', '41.60059479', '21.74527900', 8),
(192, 1, 'NIU', 'NU', 'Niue', 'Niue', 'NU-32.png', 'NU-128.png', '-19.04976362', '-169.86585571', 11),
(193, 1, 'TZA', 'TZ', 'United Republic of Tanzania', 'the United Republic of Tanzania', 'TZ-32.png', 'TZ-128.png', '-6.37551085', '34.85587302', 6),
(194, 1, 'VEN', 'VE', 'Venezuela (Bolivarian Republic of)', 'the Bolivarian Republic of Venezuela', 'VE-32.png', 'VE-128.png', '5.98477766', '-65.94152264', 6),
(195, 1, 'AIA', 'AI', 'Anguilla', 'Anguilla', 'AI-32.png', 'AI-128.png', '18.22053521', '-63.06861300', 12),
(196, 1, 'ATA', 'AQ', 'Antarctica', 'Antarctica', 'AQ-32.png', 'AQ-128.png', '-45.13806295', '10.48095703', 2),
(197, 1, 'ASM', 'AS', 'American Samoa', 'The United States Territory of American Samoa', 'AS-32.png', 'AS-128.png', '-14.30634641', '-170.69501750', 11),
(198, 1, 'ABW', 'AW', 'Aruba', 'Aruba of the Kingdom of the Netherlands', 'AW-32.png', 'AW-128.png', '12.52109661', '-69.96833800', 12),
(199, 1, 'ALA', 'AX', 'Åland Islands', 'Åland Islands', 'AX-32.png', 'AX-128.png', '60.25403213', '20.35918350', 9),
(200, 1, 'BLM', 'BL', 'Saint Barthélemy', 'Territorial collectivity of Saint Barthélemy', 'BL-32.png', 'BL-128.png', '17.90042417', '-62.83376215', 13),
(201, 1, 'BMU', 'BM', 'Bermuda', 'the Bermudas', 'BM-32.png', 'BM-128.png', '32.31995785', '-64.76182765', 12),
(202, 1, 'BES', 'BQ', 'Bonaire, Saint Eustatius And Saba', 'Bonaire, Saint Eustatius and Saba', 'BQ-32.png', 'BQ-128.png', '12.17229702', '-68.28831170', 11),
(203, 1, 'BVT', 'BV', 'Bouvet Island', 'Bouvet Island', 'BV-32.png', 'BV-128.png', '-54.42316906', '3.41319600', 12),
(204, 1, 'CCK', 'CC', 'Cocos (Keeling) Islands', 'Territory of Cocos (Keeling) Islands', 'CC-32.png', 'CC-128.png', '-12.12890685', '96.84689104', 12),
(205, 1, 'CUW', 'CW', 'Curaçao', 'Curaçao', 'CW-32.png', 'CW-128.png', '12.20710309', '-69.02160369', 11),
(206, 1, 'CXR', 'CX', 'Christmas Island', 'Territory of Christmas Island', 'CX-32.png', 'CX-128.png', '-10.49170619', '105.68083796', 11),
(207, 1, 'ESH', 'EH', 'Western Sahara', 'Western Sahara', 'EH-32.png', 'EH-128.png', '24.79324356', '-13.67683563', 6),
(208, 1, 'FLK', 'FK', 'Falkland Islands (Malvinas)', 'Falkland Islands', 'FK-32.png', 'FK-128.png', '-51.78838251', '-59.52361100', 8),
(209, 1, 'FRO', 'FO', 'Faroe Islands (Associate Member)', 'Faroe Islands', 'FO-32.png', 'FO-128.png', '61.88590482', '-6.91180400', 8),
(210, 1, 'GUF', 'GF', 'French Guiana', 'French Guiana', 'GF-32.png', 'GF-128.png', '4.01114381', '-52.97746057', 7),
(211, 1, 'GGY', 'GG', 'Guernsey', 'Bailiwick of Guernsey', 'GG-32.png', 'GG-128.png', '49.46565975', '-2.58527200', 12),
(212, 1, 'GIB', 'GI', 'Gibraltar', 'Gibraltar', 'GI-32.png', 'GI-128.png', '36.14864641', '-5.34404779', 12),
(213, 1, 'GRL', 'GL', 'Greenland', 'Greenland', 'GL-32.png', 'GL-128.png', '71.42932629', '-34.38651956', 3),
(214, 1, 'GLP', 'GP', 'Guadeloupe', 'Department of Guadeloupe', 'GP-32.png', 'GP-128.png', '16.26472785', '-61.55099400', 10),
(215, 1, 'SGS', 'GS', 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'GS-32.png', 'GS-128.png', '-54.38130284', '-36.67305304', 9),
(216, 1, 'GUM', 'GU', 'Guam', 'Territory of Guam', 'GU-32.png', 'GU-128.png', '13.44410137', '144.80747791', 10),
(217, 1, 'HKG', 'HK', 'Hong Kong', 'Hong Kong Special Administrative Region of the People''s Republic', 'HK-32.png', 'HK-128.png', '22.33728531', '114.14657786', 11),
(218, 1, 'HMD', 'HM', 'Heard Island And McDonald Islands', 'Heard and McDonald Islands', 'HM-32.png', 'HM-128.png', '-53.08168847', '73.50415800', 11),
(219, 1, 'IMN', 'IM', 'Isle of Man', 'The Isle of Man', 'IM-32.png', 'IM-128.png', '54.23562697', '-4.54805400', 10),
(220, 1, 'IOT', 'IO', 'British Indian Ocean Territory', 'The British Indian Ocean Territory', 'IO-32.png', 'IO-128.png', '-7.33461519', '72.42425280', 12),
(221, 1, 'JEY', 'JE', 'Jersey', 'Bailiwick of Jersey', 'JE-32.png', 'JE-128.png', '49.21440771', '-2.13124600', 12),
(222, 1, 'CYM', 'KY', 'Cayman Islands', 'The Cayman Islands', 'KY-32.png', 'KY-128.png', '19.31322102', '-81.25459800', 11),
(223, 1, 'MAF', 'MF', 'Saint Martin', 'Saint Martin', 'MF-32.png', 'MF-128.png', '18.07637107', '-63.05019106', 12),
(224, 1, 'MAC', 'MO', 'Macao', 'Macau Special Administrative Region', 'MO-32.png', 'MO-128.png', '22.19872287', '113.54387700', 12),
(225, 1, 'MNP', 'MP', 'Northern Mariana Islands', 'Commonwealth of the Northern Mariana Islands', 'MP-32.png', 'MP-128.png', '15.09783636', '145.67390000', 11),
(226, 1, 'MTQ', 'MQ', 'Martinique', 'Department of Martinique', 'MQ-32.png', 'MQ-128.png', '14.64128045', '-61.02417600', 10),
(227, 1, 'MSR', 'MS', 'Montserrat', 'Montserrat', 'MS-32.png', 'MS-128.png', '16.74774077', '-62.18736600', 12),
(228, 1, 'NCL', 'NC', 'New Caledonia', 'Territory of New Caledonia and Dependencies', 'NC-32.png', 'NC-128.png', '-21.26104020', '165.58783760', 8),
(229, 1, 'NFK', 'NF', 'Norfolk Island', 'Norfolk Islands', 'NF-32.png', 'NF-128.png', '-29.02801043', '167.94303023', 13),
(230, 1, 'PYF', 'PF', 'French Polynesia', 'Territory of French Polynesia', 'PF-32.png', 'PF-128.png', '-17.66243898', '-149.40683900', 10),
(231, 1, 'SPM', 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'PM-32.png', 'PM-128.png', '46.88469499', '-56.31590200', 10),
(232, 1, 'PCN', 'PN', 'Pitcairn Islands', 'Pitcairn Group of Islands', 'PN-32.png', 'PN-128.png', '-24.37673925', '-128.32423730', 13),
(233, 1, 'PRI', 'PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PR-32.png', 'PR-128.png', '18.21963053', '-66.59015100', 9),
(234, 1, 'PSE', 'PS', 'Palestinian Territory, Occupied', 'the Occupied Palestinian Territory', 'PS-32.png', 'PS-128.png', '32.26367103', '35.21936714', 8),
(235, 1, 'REU', 'RE', 'Réunion', 'Department of Reunion', 'RE-32.png', 'RE-128.png', '-21.11480084', '55.53638200', 10),
(236, 1, 'SHN', 'SH', 'Saint Helena, Ascension and Tristan da Cunha', 'Saint Helena, Ascension and Tristan da Cunha', 'SH-32.png', 'SH-128.png', '-37.10521846', '-12.27768580', 12),
(237, 1, 'SJM', 'SJ', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'SJ-32.png', 'SJ-128.png', '77.92215764', '18.99010622', 4),
(238, 1, 'SXM', 'SX', 'Sint Maarten', 'Sint Maarten', 'SX-32.png', 'SX-128.png', '18.04433885', '-63.05616320', 12),
(239, 1, 'TCA', 'TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TC-32.png', 'TC-128.png', '21.72816866', '-71.79654471', 9),
(240, 1, 'ATF', 'TF', 'French Southern and Antarctic Lands', 'Territory of the French Southern and Antarctic Lands', 'TF-32.png', 'TF-128.png', '-49.27235903', '69.34856300', 8),
(241, 1, 'TKL', 'TK', 'Tokelau (Associate Member)', 'Tokelau', 'TK-32.png', 'TK-128.png', '-9.16682644', '-171.83981693', 10),
(242, 1, 'TWN', 'TW', 'Taiwan', 'Republic of China', 'TW-32.png', 'TW-128.png', '23.71891402', '121.10884043', 7),
(243, 1, 'UMI', 'UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UM-32.png', 'UM-128.png', '19.46305694', '177.98631092', 5),
(244, 1, 'USA', 'US', 'United States of America', 'the United States of America', 'US-32.png', 'US-128.png', '37.66895362', '-102.39256450', 4),
(245, 1, 'VAT', 'VA', 'Holy See', 'Holy see', 'VA-32.png', 'VA-128.png', '41.90377810', '12.45340142', 16),
(246, 1, 'VGB', 'VG', 'Virgin Islands', 'British Virgin Islands', 'VG-32.png', 'VG-128.png', '17.67004187', '-64.77411010', 10),
(247, 1, 'VIR', 'VI', 'United States Virgin Islands', 'Virgin Islands of the United States', 'VI-32.png', 'VI-128.png', '18.01000938', '-64.77411410', 9),
(248, 1, 'WLF', 'WF', 'Wallis and Futuna', 'Territory of the Wallis and Futuna Islands', 'WF-32.png', 'WF-128.png', '-14.29378486', '-178.11649800', 12),
(249, 1, 'MYT', 'YT', 'Mayotte', 'Overseas Department of Mayotte', 'YT-32.png', 'YT-128.png', '-12.82744522', '45.16624200', 11),
(250, 1, 'SSD', 'SS', 'South Sudan', 'the Republic of South Sudan', 'SS-32.png', 'SS-128.png', '7.91320803', '30.15342434', 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `country_region`
--

CREATE TABLE IF NOT EXISTS `country_region` (
  `id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `country_region`
--

INSERT INTO `country_region` (`id`, `country_id`, `region_id`) VALUES
(2, 1, 2),
(4, 2, 3),
(5, 3, 1),
(7, 4, 3),
(8, 5, 1),
(11, 6, 4),
(12, 7, 4),
(13, 8, 3),
(15, 9, 2),
(16, 10, 3),
(17, 11, 3),
(18, 12, 4),
(19, 13, 7),
(20, 14, 2),
(21, 15, 4),
(22, 16, 3),
(23, 17, 3),
(24, 18, 4),
(25, 19, 1),
(27, 20, 2),
(30, 21, 3),
(31, 22, 1),
(33, 23, 4),
(35, 24, 2),
(36, 25, 3),
(37, 26, 1),
(38, 27, 1),
(39, 28, 2),
(40, 29, 1),
(41, 30, 5),
(252, 30, 6),
(42, 31, 1),
(44, 32, 1),
(45, 33, 1),
(46, 34, 4),
(47, 35, 2),
(50, 36, 4),
(51, 37, 1),
(52, 38, 1),
(54, 39, 4),
(56, 40, 3),
(57, 41, 4),
(59, 42, 3),
(60, 43, 3),
(55, 44, 1),
(254, 45, 3),
(62, 45, 6),
(63, 46, 1),
(64, 47, 4),
(65, 48, 4),
(66, 49, 4),
(67, 50, 1),
(68, 51, 4),
(69, 52, 1),
(70, 53, 1),
(71, 54, 3),
(72, 55, 1),
(76, 56, 2),
(255, 57, 3),
(77, 57, 6),
(78, 58, 3),
(82, 59, 1),
(222, 60, 1),
(83, 61, 3),
(84, 62, 3),
(85, 63, 1),
(87, 64, 3),
(89, 65, 4),
(92, 66, 4),
(94, 67, 1),
(95, 68, 1),
(96, 69, 4),
(97, 70, 4),
(99, 71, 4),
(101, 72, 3),
(256, 73, 3),
(102, 73, 6),
(103, 74, 2),
(104, 75, 2),
(106, 76, 7),
(107, 77, 3),
(109, 78, 3),
(110, 79, 3),
(111, 80, 4),
(112, 81, 2),
(114, 82, 7),
(115, 83, 3),
(116, 84, 1),
(117, 85, 2),
(118, 86, 7),
(119, 87, 3),
(120, 88, 2),
(121, 89, 3),
(122, 90, 7),
(123, 91, 1),
(124, 92, 1),
(125, 93, 1),
(126, 94, 3),
(127, 95, 3),
(128, 96, 3),
(131, 97, 1),
(132, 98, 1),
(133, 99, 2),
(134, 100, 2),
(135, 101, 1),
(136, 102, 3),
(137, 103, 2),
(139, 104, 1),
(140, 105, 1),
(142, 106, 4),
(145, 107, 3),
(146, 108, 2),
(147, 109, 3),
(149, 110, 1),
(150, 111, 1),
(151, 112, 2),
(152, 113, 1),
(153, 114, 2),
(154, 115, 2),
(155, 116, 3),
(157, 117, 2),
(158, 118, 4),
(159, 119, 1),
(160, 120, 1),
(257, 121, 3),
(165, 121, 6),
(166, 122, 7),
(167, 123, 2),
(168, 124, 2),
(170, 125, 4),
(171, 126, 2),
(172, 127, 4),
(173, 128, 4),
(174, 129, 2),
(176, 130, 3),
(177, 131, 3),
(179, 132, 7),
(181, 133, 3),
(258, 134, 3),
(182, 134, 6),
(183, 135, 1),
(186, 136, 4),
(187, 137, 4),
(190, 138, 4),
(191, 139, 2),
(192, 140, 3),
(193, 141, 1),
(194, 142, 7),
(195, 143, 1),
(196, 144, 3),
(197, 145, 1),
(198, 146, 1),
(199, 147, 2),
(201, 148, 3),
(202, 149, 3),
(203, 150, 2),
(204, 151, 1),
(205, 152, 1),
(209, 153, 3),
(210, 154, 2),
(211, 155, 1),
(212, 156, 4),
(214, 157, 1),
(259, 158, 3),
(215, 158, 6),
(216, 159, 3),
(217, 160, 7),
(219, 161, 3),
(221, 162, 2),
(223, 163, 2),
(224, 164, 1),
(226, 165, 2),
(227, 166, 4),
(228, 167, 1),
(229, 168, 3),
(230, 169, 3),
(232, 170, 2),
(233, 171, 1),
(234, 172, 3),
(235, 173, 7),
(240, 174, 4),
(241, 175, 3),
(242, 176, 2),
(245, 177, 2),
(249, 178, 7),
(250, 179, 1),
(251, 180, 1),
(53, 181, 2),
(28, 182, 4),
(61, 183, 1),
(73, 184, 3),
(143, 185, 2),
(236, 186, 3),
(105, 187, 2),
(163, 188, 2),
(207, 189, 2),
(144, 190, 3),
(130, 191, 3),
(161, 192, 2),
(220, 193, 1),
(244, 194, 4),
(9, 195, 4),
(6, 197, 2),
(14, 198, 4),
(3, 199, 3),
(184, 200, 4),
(26, 201, 5),
(29, 202, 4),
(58, 205, 4),
(248, 207, 1),
(74, 208, 4),
(75, 209, 6),
(79, 210, 3),
(93, 211, 3),
(86, 212, 3),
(88, 213, 6),
(90, 214, 4),
(91, 216, 2),
(100, 217, 2),
(108, 219, 3),
(113, 221, 1),
(43, 222, 4),
(188, 223, 4),
(129, 224, 2),
(164, 225, 2),
(138, 226, 4),
(148, 227, 4),
(156, 228, 2),
(162, 229, 2),
(80, 230, 2),
(189, 231, 5),
(175, 232, 2),
(178, 233, 4),
(169, 234, 7),
(180, 235, 1),
(185, 236, 1),
(213, 237, 3),
(200, 238, 4),
(231, 239, 4),
(225, 241, 2),
(237, 244, 5),
(253, 244, 6),
(243, 245, 1),
(246, 246, 4),
(239, 247, 4),
(247, 248, 2),
(141, 249, 1),
(208, 250, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ingredient`
--

CREATE TABLE IF NOT EXISTS `ingredient` (
  `id` int(11) NOT NULL,
  `id_ingredient_type` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carbohydrate_g` double DEFAULT NULL,
  `protein_g` double DEFAULT NULL,
  `saturated_fat_g` double DEFAULT NULL,
  `fat_g` double DEFAULT NULL,
  `poly_unsaturated_fat_g` double DEFAULT NULL,
  `mono_unsaturated_fat_g` double DEFAULT NULL,
  `trans_fat_g` double DEFAULT NULL,
  `water_g` double DEFAULT NULL,
  `kcal_g` double DEFAULT NULL,
  `cholesterol_g` double DEFAULT NULL,
  `potassium_g` double DEFAULT NULL,
  `sodium_g` double DEFAULT NULL,
  `calcium_g` double DEFAULT NULL,
  `iron_g` double DEFAULT NULL,
  `magnesium_g` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ingredients_of_recipes`
--

CREATE TABLE IF NOT EXISTS `ingredients_of_recipes` (
  `recipe_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ingredients_vitamins`
--

CREATE TABLE IF NOT EXISTS `ingredients_vitamins` (
  `ingredient_id` int(11) NOT NULL,
  `vitamin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ingredient_type`
--

CREATE TABLE IF NOT EXISTS `ingredient_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `recipe`
--

CREATE TABLE IF NOT EXISTS `recipe` (
  `id` int(11) NOT NULL,
  `id_category` int(11) DEFAULT NULL,
  `id_dish` int(11) DEFAULT NULL,
  `id_festivity` int(11) DEFAULT NULL,
  `id_country` int(11) DEFAULT NULL,
  `id_recipe_type` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cost` double NOT NULL,
  `working_time` datetime NOT NULL,
  `cooking_time` datetime NOT NULL,
  `total_time` datetime NOT NULL,
  `n_person_dose` int(11) NOT NULL,
  `procedure` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `advice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `veg` tinyint(1) DEFAULT NULL,
  `gluten_free` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `recipe_category`
--

CREATE TABLE IF NOT EXISTS `recipe_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `recipe_dish`
--

CREATE TABLE IF NOT EXISTS `recipe_dish` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `recipe_festivity`
--

CREATE TABLE IF NOT EXISTS `recipe_festivity` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `recipe_of_recipe`
--

CREATE TABLE IF NOT EXISTS `recipe_of_recipe` (
  `id_recipe` int(11) NOT NULL,
  `id_recipe_of_recipe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `recipe_type`
--

CREATE TABLE IF NOT EXISTS `recipe_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_unep` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `region`
--

INSERT INTO `region` (`id`, `name`, `is_unep`) VALUES
(1, 'Africa', 1),
(2, 'Asia and the Pacific', 1),
(3, 'Europe', 1),
(4, 'Latin America and the Caribbean', 1),
(5, 'North America', 1),
(6, 'Polar: Arctic', 0),
(7, 'West Asia', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(11) NOT NULL,
  `id_recipe` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `revies_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `judgment` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `tool`
--

CREATE TABLE IF NOT EXISTS `tool` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `tools_of_recipes`
--

CREATE TABLE IF NOT EXISTS `tools_of_recipes` (
  `recipe_id` int(11) NOT NULL,
  `tool_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `id_country` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `vitamin`
--

CREATE TABLE IF NOT EXISTS `vitamin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_countries_name` (`name`),
  ADD UNIQUE KEY `idx_countries_code3l` (`code3l`),
  ADD UNIQUE KEY `idx_countries_code2l` (`code2l`);

--
-- Indici per le tabelle `country_region`
--
ALTER TABLE `country_region`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `country_id` (`country_id`,`region_id`),
  ADD KEY `fk_country_region__region_idx` (`region_id`),
  ADD KEY `IDX_4F1A1A05F92F3E70` (`country_id`);

--
-- Indici per le tabelle `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6BAF78703BD12437` (`id_ingredient_type`);

--
-- Indici per le tabelle `ingredients_of_recipes`
--
ALTER TABLE `ingredients_of_recipes`
  ADD PRIMARY KEY (`recipe_id`,`ingredient_id`),
  ADD KEY `IDX_981FBB9C59D8A214` (`recipe_id`),
  ADD KEY `IDX_981FBB9C933FE08C` (`ingredient_id`);

--
-- Indici per le tabelle `ingredients_vitamins`
--
ALTER TABLE `ingredients_vitamins`
  ADD PRIMARY KEY (`ingredient_id`,`vitamin_id`),
  ADD KEY `IDX_E839E8F5933FE08C` (`ingredient_id`),
  ADD KEY `IDX_E839E8F58F77E7C7` (`vitamin_id`);

--
-- Indici per le tabelle `ingredient_type`
--
ALTER TABLE `ingredient_type`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DA88B1375697F554` (`id_category`),
  ADD KEY `IDX_DA88B1371E5D90BA` (`id_dish`),
  ADD KEY `IDX_DA88B137638714A0` (`id_festivity`),
  ADD KEY `IDX_DA88B1378DEE6016` (`id_country`),
  ADD KEY `IDX_DA88B137EBD7C001` (`id_recipe_type`),
  ADD KEY `IDX_DA88B1376B3CA4B` (`id_user`);

--
-- Indici per le tabelle `recipe_category`
--
ALTER TABLE `recipe_category`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `recipe_dish`
--
ALTER TABLE `recipe_dish`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `recipe_festivity`
--
ALTER TABLE `recipe_festivity`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `recipe_of_recipe`
--
ALTER TABLE `recipe_of_recipe`
  ADD PRIMARY KEY (`id_recipe`,`id_recipe_of_recipe`),
  ADD KEY `IDX_DA4154CBFCBF04DA` (`id_recipe`),
  ADD KEY `IDX_DA4154CB39CB011A` (`id_recipe_of_recipe`);

--
-- Indici per le tabelle `recipe_type`
--
ALTER TABLE `recipe_type`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_794381C6FCBF04DA` (`id_recipe`),
  ADD KEY `IDX_794381C66B3CA4B` (`id_user`);

--
-- Indici per le tabelle `tool`
--
ALTER TABLE `tool`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `tools_of_recipes`
--
ALTER TABLE `tools_of_recipes`
  ADD PRIMARY KEY (`recipe_id`,`tool_id`),
  ADD KEY `IDX_BA5E6B5B59D8A214` (`recipe_id`),
  ADD KEY `IDX_BA5E6B5B8F7B22CC` (`tool_id`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD KEY `IDX_8D93D6498DEE6016` (`id_country`);

--
-- Indici per le tabelle `vitamin`
--
ALTER TABLE `vitamin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT per la tabella `country_region`
--
ALTER TABLE `country_region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=260;
--
-- AUTO_INCREMENT per la tabella `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ingredient_type`
--
ALTER TABLE `ingredient_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `recipe_category`
--
ALTER TABLE `recipe_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `recipe_dish`
--
ALTER TABLE `recipe_dish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `recipe_festivity`
--
ALTER TABLE `recipe_festivity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `recipe_type`
--
ALTER TABLE `recipe_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT per la tabella `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `tool`
--
ALTER TABLE `tool`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `vitamin`
--
ALTER TABLE `vitamin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `country_region`
--
ALTER TABLE `country_region`
  ADD CONSTRAINT `FK_4F1A1A0598260155` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`),
  ADD CONSTRAINT `FK_4F1A1A05F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Limiti per la tabella `ingredient`
--
ALTER TABLE `ingredient`
  ADD CONSTRAINT `FK_6BAF78703BD12437` FOREIGN KEY (`id_ingredient_type`) REFERENCES `ingredient_type` (`id`);

--
-- Limiti per la tabella `ingredients_of_recipes`
--
ALTER TABLE `ingredients_of_recipes`
  ADD CONSTRAINT `FK_981FBB9C59D8A214` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_981FBB9C933FE08C` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE;

--
-- Limiti per la tabella `ingredients_vitamins`
--
ALTER TABLE `ingredients_vitamins`
  ADD CONSTRAINT `FK_E839E8F58F77E7C7` FOREIGN KEY (`vitamin_id`) REFERENCES `vitamin` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_E839E8F5933FE08C` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE;

--
-- Limiti per la tabella `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `FK_DA88B1371E5D90BA` FOREIGN KEY (`id_dish`) REFERENCES `recipe_dish` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_DA88B1375697F554` FOREIGN KEY (`id_category`) REFERENCES `recipe_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_DA88B137638714A0` FOREIGN KEY (`id_festivity`) REFERENCES `recipe_festivity` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_DA88B1376B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_DA88B1378DEE6016` FOREIGN KEY (`id_country`) REFERENCES `country` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_DA88B137EBD7C001` FOREIGN KEY (`id_recipe_type`) REFERENCES `recipe_type` (`id`) ON DELETE SET NULL;

--
-- Limiti per la tabella `recipe_of_recipe`
--
ALTER TABLE `recipe_of_recipe`
  ADD CONSTRAINT `FK_DA4154CB39CB011A` FOREIGN KEY (`id_recipe_of_recipe`) REFERENCES `recipe` (`id`),
  ADD CONSTRAINT `FK_DA4154CBFCBF04DA` FOREIGN KEY (`id_recipe`) REFERENCES `recipe` (`id`);

--
-- Limiti per la tabella `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `FK_794381C66B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_794381C6FCBF04DA` FOREIGN KEY (`id_recipe`) REFERENCES `recipe` (`id`);

--
-- Limiti per la tabella `tools_of_recipes`
--
ALTER TABLE `tools_of_recipes`
  ADD CONSTRAINT `FK_BA5E6B5B59D8A214` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_BA5E6B5B8F7B22CC` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`) ON DELETE CASCADE;

--
-- Limiti per la tabella `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D6498DEE6016` FOREIGN KEY (`id_country`) REFERENCES `country` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

#!/usr/bin/env bash
php app/console doctrine:schema:update --dump-sql
php app/console doctrine:generate:entities RecipeBookCoreBundle --no-backup
php app/console doctrine:schema:update --force
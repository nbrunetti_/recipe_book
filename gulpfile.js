//PLUGINS
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var del = require('del');
var Q = require('q');
var shell = require('gulp-shell');
var debug = require('gulp-debug');
var fs = require('fs');

//CUSTOM VARS
var publicDir = 'web/assets/';
var bowerDir = 'vendor/bower_components/';
var webBundleDir = 'web/bundles/';
var rootDir = 'app/Resources/public/';
var vendorDir = 'vendor/';

var config = {
    vendorDir: vendorDir,
    publicDir: publicDir,
    jsDir: publicDir+'js/',
    cssDir: publicDir+'css/',
    bowerDir: bowerDir,
    fontsDir: publicDir+'font',
    webBundleDir: webBundleDir,
    rootDir: rootDir,
    production: !!plugins.util.env.production,
    sourceMaps: !plugins.util.env.production
};
var app = {};

app.addStyle = function(paths, outputFilename) {
    return gulp.src(paths)
        .pipe(debug({title: 'styles:'}))
        .pipe(plugins.plumber())
        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.init()))
        //.pipe(plugins.sass())
        .pipe(plugins.concat(outputFilename))
        .pipe(config.production ? plugins.minifyCss() : plugins.util.noop())
        //.pipe(plugins.rev())
        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.write('.')))
        .pipe(gulp.dest(config.cssDir));
    // write the rev-manifest.json file for gulp-rev
    //.pipe(plugins.rev.manifest(config.revManifestPath, {
    //    merge: true
    //}))
    //.pipe(gulp.dest('.'));
};

app.addScript = function(paths, outputFilename) {
    return gulp.src(paths)
        .pipe(debug({title: 'scripts:'}))
        .pipe(plugins.plumber())
        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.init()))
        .pipe(plugins.concat(outputFilename))
        .pipe(config.production ? plugins.uglify() : plugins.util.noop())
        //.pipe(plugins.rev())
        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.write('.')))
        .pipe(gulp.dest(config.jsDir));
    // write the rev-manifest.json file for gulp-rev
    //.pipe(plugins.rev.manifest(config.revManifestPath, {
    //    merge: true
    //}))
    //.pipe(gulp.dest('./merged'));
};

app.copy = function(srcFiles, outputDir) {
    return gulp.src(srcFiles)
        .pipe(gulp.dest(outputDir));
};

var Pipeline = function() {
    this.entries = [];
};
Pipeline.prototype.add = function() {
    this.entries.push(arguments);
};
Pipeline.prototype.run = function(callable) {
    var deferred = Q.defer();
    var i = 0;
    var entries = this.entries;

    var runNextEntry = function() {
        // see if we're all done looping
        if (typeof entries[i] === 'undefined') {
            deferred.resolve();
            return;
        }

        // pass app as this, though we should avoid using "this"
        // in those functions anyways
        callable.apply(app, entries[i]).on('end', function() {
            i++;
            runNextEntry();
        });
    };
    runNextEntry();

    return deferred.promise;
};

/*
 ------------------ DA QUI INIZIA IL CODICE OPERATIVO
 */

gulp.task('styles', function() {
    var pipeline = new Pipeline();

    pipeline.add([
        config.rootDir          + 'css/frontend.css',
        config.bowerDir         + 'bootstrap/dist/css/bootstrap.min.css',
        config.bowerDir         + 'bootstrap3-dialog/dist/css/bootstrap-dialog.min.css',
        config.bowerDir         + 'font-awesome/css/font-awesome.min.css',
        config.bowerDir         + 'jquery-ui/themes/base/jquery-ui.min.css',
        config.bowerDir         + 'select2/dist/css/select2.min.css',

    ], 'frontend.css');

    /*pipeline.add([
        config.bowerDir+'bootstrap/dist/css/bootstrap.min.css',
        config.adminBundleDir+'backend.css',
    ], 'backend.css');*/

    return pipeline.run(app.addStyle);
});

gulp.task('scripts',function() { // mettendo ['styles'] forziamo questo task ad aspettare che finisca il task style
    var pipeline = new Pipeline();

    pipeline.add([
        config.bowerDir+'jquery/dist/jquery.min.js',
        config.bowerDir+'bootstrap/dist/js/bootstrap.min.js',
        config.bowerDir+'select2/dist/js/select2.full.min.js',
        config.rootDir+'js/*'
    ], 'frontend.js');

    pipeline.add([
        config.bowerDir+'jquery/dist/jquery.min.js',
        config.bowerDir+'bootstrap/dist/js/bootstrap.min.js',
        config.bowerDir+'select2/dist/js/select2.full.min.js',
        config.rootDir+'js/*'
    ], 'backend.js');

    return pipeline.run(app.addScript);
});

gulp.task('fonts', function() {
    var pipeline = new Pipeline();

    app.copy(
        config.bowerDir + 'bootstrap/dist/fonts/*',
        config.publicDir + '/fonts/'
    );
    app.copy(
        config.rootDir + '/fonts/flaticon/Flaticon.woff',
        config.publicDir + '/fonts/'
    );
    app.copy(
        config.rootDir + '/fonts/flaticon/Flaticon.ttf',
        config.publicDir + '/fonts/'
    );
    app.copy(
        config.rootDir + '/fonts/flaticon/Flaticon.eot',
        config.publicDir + '/fonts/'
    );
    app.copy(
        config.rootDir + '/fonts/flaticon/Flaticon.svg',
        config.publicDir + '/fonts/'
    );
    app.copy(
        config.rootDir + '/fonts/flaticon2/Flaticon2.woff',
        config.publicDir + '/fonts/'
    );
    app.copy(
        config.rootDir + '/fonts/flaticon2/Flaticon2.ttf',
        config.publicDir + '/fonts/'
    );
    app.copy(
        config.rootDir + '/fonts/flaticon2/Flaticon2.eot',
        config.publicDir + '/fonts/'
    );
    app.copy(
        config.rootDir + '/fonts/flaticon2/Flaticon2.svg',
        config.publicDir + '/fonts/'
    );
    app.copy(
        config.rootDir + 'font-awesome/fonts/*',
        config.publicDir + '/fonts/'
    );

    return pipeline.add(['/fonts/']);
});

gulp.task('clean', function() {
    //del.sync(config.revManifestPath);
    del.sync(config.jsDir);
    del.sync(config.cssDir);
});

gulp.task('watch', function() {
    gulp.watch(config.rootDir+'css/*.css', ['styles']);
});

gulp.task('default', ['clean','fonts','styles','scripts','watch' ]);
//gulp.task('default', ['scripts' ]);

//Debug file path
//fs.stat(config.webBundleDir+'fruitloopapp/css/frontend.css', function(err, stat) {
//    if(err == null) {
//        console.log('File exists');
//    } else {
//        console.log(err.code);
//    }
//});




//app.addStyleNoConcat = function(paths, destination) {
//    return gulp.src(paths)
//        .pipe(plugins.plumber())
//        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.init()))
//        .pipe(plugins.sass())
//        .pipe(config.production ? plugins.minifyCss() : plugins.util.noop())
//        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.write('.')))
//        .pipe(gulp.dest(destination));
//};



//gulp.task('styles_no_concat', function () {
//    var pipeline = new Pipeline();
//
//    pipeline.add([
//        'sass/*.*'
//    ],config.publicDir+'/css');
//
//    return pipeline.run(app.addStyleNoConcat);
//});
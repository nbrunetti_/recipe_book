#!/usr/bin/env bash
chmod -R 777 app/cache
chmod -R 777 app/logs
php app/console cache:clear --env=dev --no-debug
php app/console cache:clear --env=prod --no-debug
php app/console fos:js-routing:dump
php app/console assets:install --symlink web
chmod -R 777 app/cache
chmod -R 777 app/logs

<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 04/02/2017
 * Time: 17:23
 */

namespace RecipeBook\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

class RecipeRepository extends EntityRepository
{

    /**
     * @return \Doctrine\ORM\Query
     */
    public function findLastTenInsertedRecipe(){
        $qb = $this->createQueryBuilder('recipe');

        $qb
            ->orderBy('recipe.insertedAt', 'DESC')
            ->setMaxResults(10);

        return $qb->getQuery();
    }

    /**
     * @param $user
     * @return \Doctrine\ORM\Query
     */
    public function findUserLastTenInsertedRecipe($user){
        $qb = $this->createQueryBuilder('recipe');

        $qb
            ->where('recipe.user = :user')
            ->orderBy('recipe.name', 'ASC')
            ->setParameter('user', $user)
            ->setMaxResults(10);

        return $qb->getQuery();
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllRecipeDishByParameters($recipeDish){
        $qb = $this->createQueryBuilder('recipe');

        $qb
            ->where('recipe.dish = :dish')
            ->orderBy('recipe.name', 'ASC')
            ->setParameter('dish', $recipeDish);

        return $qb->getQuery();
    }

    public function getAllRecipeByParameters($paramName, $param){
        $qb = $this->createQueryBuilder('recipe');

        $qb
            ->where('recipe.' . $paramName .' = :param')
            ->orderBy('recipe.name', 'ASC')
            ->setParameter('param', $param);

        return $qb->getQuery();
    }



//    public function getTopTenRecipe(){
//        $qb = $this->createQueryBuilder('recipe');
//
//        $qb
//            ->orderBy('recipe.judgement', 'ASC')
//            ->orderBy('')
//
//    }

}
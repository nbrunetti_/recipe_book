<?php
/**
 * Created by PhpStorm.
 * User: quadra
 * Date: 09/01/17
 * Time: 11:29
 */

namespace RecipeBook\CoreBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ToolRepository extends EntityRepository
{
    public function findsAllTools(){
        $qb = $this->createQueryBuilder('t');

        return $qb
            ->select('t')
            ->getQuery();
    }

}
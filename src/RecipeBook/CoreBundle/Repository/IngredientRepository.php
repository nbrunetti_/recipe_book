<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 08/01/2017
 * Time: 18:33
 */

namespace RecipeBook\CoreBundle\Repository;


use Doctrine\ORM\EntityRepository;

class IngredientRepository extends EntityRepository
{
    /**
     * Find all ingredient without parameters
     *
     * @return \Doctrine\ORM\Query
     */
    public function findAllIngredients(){
        $qb = $this->createQueryBuilder('i');

        return $qb
            ->select('i')
            ->getQuery();
    }
}
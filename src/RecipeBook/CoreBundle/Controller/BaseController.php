<?php

namespace RecipeBook\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class BaseController extends Controller
{
    const FLASH_MESSAGE_NOTICE = "notice";
    const FLASH_MESSAGE_WARNING = "warning";
    const FLASH_MESSAGE_ERROR = "error";

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    public function em(){
        return $this->getDoctrine()->getManager();
    }

    /**
     * @return mixed
     */
    public function getUser(){
        return parent::getUser();
    }

    public function getPaginator(){
        return $this->get('knp_paginator');
    }

    /**
     * ShortCut per il redirect alla path di un controller
     *
     * @param      $route
     * @param null $parameters
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function routeRedirect($route, $parameters = NULL)
    {
        return $this->redirect($parameters ? $this->generateUrl($route, $parameters) : $this->generateUrl($route));
    }

    public function flash($message, $key = NULL)
    {
        $this->get('session')->getFlashBag()->add(
            $key ? $key : 'notice',
            $message
        );
    }

    /**
     * @param null $key
     * @param bool $only_first_result
     * @return array|mixed|string
     */
    public function getFlash($key = NULL, $only_first_result = false)
    {
        $flash = $this->get('session')->getFlashBag()->get($key ? $key : 'notice',[]);
        if($only_first_result && is_array($flash)){
            $flash = count($flash) ? $flash[0] : "";
        }
        return $flash;
    }

    /**
     * @param Request $request
     * @return Session
     */
    public function getSession(Request $request)
    {
        $session = $request->getSession();
        if( null === $session ){
            $session = new Session();
            $session->start();
        }
        return $session;
    }

    /**
     * @return bool
     */
    public function isUserLoggedIn()
    {
        return $this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED');
    }
}

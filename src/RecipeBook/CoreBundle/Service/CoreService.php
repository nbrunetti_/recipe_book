<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 29/01/2017
 * Time: 17:51
 */

namespace RecipeBook\CoreBundle\Service;


use Doctrine\ORM\EntityManager;
use RecipeBook\CoreBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

abstract class CoreService
{
    /** @var EntityManager  */
    protected$em;

    /** @var ContainerInterface  */
    protected $container;

    /** @var Router  */
    protected $route;

    /** @var FormFactory  */
    protected $formFactory;


    /**
     * CoreService constructor.
     * @param EntityManager $em
     * @param ContainerInterface $container
     * @param Router $router
     * @param \Symfony\Component\Form\FormFactory $formFactory
     */
    public function __construct(EntityManager $em, ContainerInterface $container, Router $router, FormFactory $formFactory)
    {
        $this->em = $em;
        $this->container = $container;
        $this->router = $router;
        $this->formFactory = $formFactory;
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @return User|null
     * @throws \LogicException If SecurityBundle is not available
     * @see TokenInterface::getUser()
     */
    protected function getUser()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return null;
        }

        /** @var TokenInterface $token */
        if (!is_object($user = $token->getUser())) {
            return null;
        }
        return $user;
    }
}
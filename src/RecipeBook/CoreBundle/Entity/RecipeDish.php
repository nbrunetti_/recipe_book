<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 16/07/2016
 * Time: 03:44
 */

namespace RecipeBook\CoreBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="recipe_dish")
 */
 class RecipeDish
 {
     /**
      * @var int
      * @ORM\Id
      * @ORM\Column(type="integer")
      * @ORM\GeneratedValue(strategy="AUTO")
      */
     protected $id;

     /**
      * @var string
      * @ORM\Column(type="string", nullable=true)
      */
     protected $name;

     /**
      * @var string
      * @ORM\Column(type="string", nullable=true)
      */
     protected $pluralName;

     /**
      * @var ArrayCollection
      * @ORM\OneToMany(targetEntity="RecipeBook\CoreBundle\Entity\Recipe", mappedBy="dish")
      */
     protected $recipes;


     public function __construct()
     {
         $this->recipe = new ArrayCollection();
     }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set singularName
     *
     * @param string $name
     * @return RecipeDish
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get singularName
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set pluralName
     *
     * @param string $pluralName
     * @return RecipeDish
     */
    public function setPluralName($pluralName)
    {
        $this->pluralName = $pluralName;

        return $this;
    }

    /**
     * Get pluralName
     *
     * @return string 
     */
    public function getPluralName()
    {
        return $this->pluralName;
    }

    /**
     * Add recipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $recipes
     * @return RecipeDish
     */
    public function addRecipe(\RecipeBook\CoreBundle\Entity\Recipe $recipes)
    {
        $this->recipes[] = $recipes;

        return $this;
    }

    /**
     * Remove recipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $recipes
     */
    public function removeRecipe(\RecipeBook\CoreBundle\Entity\Recipe $recipes)
    {
        $this->recipes->removeElement($recipes);
    }

    /**
     * Get recipes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecipes()
    {
        return $this->recipes;
    }
}

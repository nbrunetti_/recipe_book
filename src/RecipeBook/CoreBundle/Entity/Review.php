<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 19/07/2016
 * Time: 14:04
 */

namespace RecipeBook\CoreBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="review")
 */
 class Review
 {
     /**
      * @var integer
      * @ORM\Id
      * @ORM\Column(type="integer")
      * @ORM\GeneratedValue(strategy="AUTO")
      */
     protected $id;

     /**
      * @var string
      * @ORM\Column(type="string", nullable=true)
      */
     protected $reviewsText;

     /**
      * @var integer
      * @ORM\Column(type="integer", nullable=true)
      */
     protected $judgement;

     /**
      * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\Recipe", inversedBy="reviews")
      * @ORM\JoinColumn(name="id_recipe", referencedColumnName="id")
      */
     protected $recipes;

     /**
      * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\User", inversedBy="reviews")
      * @ORM\JoinColumn(name="id_user", referencedColumnName="id")
      */
     protected $users;

     /**
      * @ORM\Column(type="datetime")
      *
      * @var \DateTime
      */
     protected $insertedAt;

     public function __construct()
     {
         $this->insertedAt = new \DateTime();
     }

     /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $recipes
     * @return Review
     */
    public function setRecipes(\RecipeBook\CoreBundle\Entity\Recipe $recipes = null)
    {
        $this->recipes = $recipes;

        return $this;
    }

    /**
     * Get recipes
     *
     * @return \RecipeBook\CoreBundle\Entity\Recipe 
     */
    public function getRecipes()
    {
        return $this->recipes;
    }

    /**
     * Set users
     *
     * @param \RecipeBook\CoreBundle\Entity\User $users
     * @return Review
     */
    public function setUsers(\RecipeBook\CoreBundle\Entity\User $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \RecipeBook\CoreBundle\Entity\User 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set insertedAt
     *
     * @param \DateTime $insertedAt
     * @return Review
     */
    public function setInsertedAt($insertedAt)
    {
        $this->insertedAt = $insertedAt;

        return $this;
    }

    /**
     * Get insertedAt
     *
     * @return \DateTime 
     */
    public function getInsertedAt()
    {
        return $this->insertedAt;
    }

    /**
     * Set reviewsText
     *
     * @param string $reviewsText
     * @return Review
     */
    public function setReviewsText($reviewsText)
    {
        $this->reviewsText = $reviewsText;

        return $this;
    }

    /**
     * Get reviewsText
     *
     * @return string 
     */
    public function getReviewsText()
    {
        return $this->reviewsText;
    }

    /**
     * Set judgement
     *
     * @param integer $judgement
     * @return Review
     */
    public function setJudgement($judgement)
    {
        $this->judgement = $judgement;

        return $this;
    }

    /**
     * Get judgement
     *
     * @return integer 
     */
    public function getJudgement()
    {
        return $this->judgement;
    }
}

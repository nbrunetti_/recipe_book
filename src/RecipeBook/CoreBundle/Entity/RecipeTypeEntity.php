<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 16/07/2016
 * Time: 14:11
 */

namespace RecipeBook\CoreBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="recipe_type")
 */
 class RecipeTypeEntity
 {
     /**
      * @var int
      * @ORM\Id
      * @ORM\Column(type="integer")
      * @ORM\GeneratedValue(strategy="AUTO")
      */
     protected $id;

     /**
      * @var string
      * @ORM\Column(type="string", nullable=true)
      */
     protected $name;

     /**
      * @var string
      * @ORM\Column(type="string", nullable=true)
      */
     protected $description;

     /**
      * @var ArrayCollection
      * @ORM\OneToMany(targetEntity="RecipeBook\CoreBundle\Entity\Recipe", mappedBy="description")
      */
     protected $recipes;

     public function __construct()
     {
         $this->recipes = new ArrayCollection();
     }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RecipeTypeEntity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return RecipeTypeEntity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add recipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $recipes
     * @return RecipeTypeEntity
     */
    public function addRecipe(\RecipeBook\CoreBundle\Entity\Recipe $recipes)
    {
        $this->recipes[] = $recipes;

        return $this;
    }

    /**
     * Remove recipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $recipes
     */
    public function removeRecipe(\RecipeBook\CoreBundle\Entity\Recipe $recipes)
    {
        $this->recipes->removeElement($recipes);
    }

    /**
     * Get recipes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecipes()
    {
        return $this->recipes;
    }
}

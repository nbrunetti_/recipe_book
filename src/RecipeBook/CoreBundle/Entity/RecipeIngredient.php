<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 02/02/2017
 * Time: 04:26
 */

namespace RecipeBook\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class RecipeIngredient
 * @package RecipeBook\CoreBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="recipe_ingredient")
 */
class RecipeIngredient
{
    /** @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\Ingredient", inversedBy="ingredientOfRecipe", cascade={"all"})
     * @ORM\JoinColumn(name="id_ingredient", referencedColumnName="id")
     */
    protected $ingredient;

    /**
     * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\Recipe", inversedBy="recipeIngredients", cascade={"all"})
     * @ORM\JoinColumn(name="id_recipe", referencedColumnName="id")
     */
    protected $recipe;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $quantity;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ingredient
     *
     * @param \RecipeBook\CoreBundle\Entity\Ingredient $ingredient
     * @return RecipeIngredient
     */
    public function setIngredient(\RecipeBook\CoreBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient
     *
     * @return \RecipeBook\CoreBundle\Entity\Ingredient
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * Set recipe
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $recipe
     * @return RecipeIngredient
     */
    public function setRecipe(\RecipeBook\CoreBundle\Entity\Recipe $recipe = null)
    {
        $this->recipe = $recipe;

        return $this;
    }

    /**
     * Get recipe
     *
     * @return \RecipeBook\CoreBundle\Entity\Recipe
     */
    public function getRecipe()
    {
        return $this->recipe;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return RecipeIngredient
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}

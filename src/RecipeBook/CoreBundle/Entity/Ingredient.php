<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 16/07/2016
 * Time: 14:47
 */

namespace RecipeBook\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ingredient")
 * @ORM\Entity(repositoryClass="RecipeBook\CoreBundle\Repository\IngredientRepository")
 */
 class Ingredient
 {
     /**
      * @var integer
      * @ORM\Id
      * @ORM\Column(type="integer")
      * @ORM\GeneratedValue(strategy="AUTO")
      */
     protected $id;

     /** @var string
      *  @ORM\Column(type="string")
      */
     protected $name;

     /**
      * @var string
      * @ORM\Column(type="string", nullable=true)
      */
     protected $description;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $carbohydrateG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $proteinG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $fatG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $saturatedFatG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $polyUnsaturatedFatG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $monoUnsaturatedFatG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $transFatG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $kcalG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $cholesterolG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $potassiumG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $sodiumG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $calciumG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $ironG;

     /**
      * @var float
      * @ORM\Column(type="float", nullable=true)
      */
     protected $magnesiumG;

     /**
      * @ORM\ManyToMany(targetEntity="RecipeBook\CoreBundle\Entity\Vitamin", inversedBy="ingredients")
      * @ORM\JoinTable(name="ingredients_vitamins")
      */
     protected $vitamins;

     /**
      * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\IngredientTypeEntity", inversedBy="ingredients")
      * @ORM\JoinColumn(name="id_ingredient_type", referencedColumnName="id")
      */
     protected $ingredientType;

     /**
      * @var ArrayCollection
      * @ORM\OneToMany(targetEntity="RecipeBook\CoreBundle\Entity\RecipeIngredient", mappedBy="ingredient")
      */
     protected $ingredientOfRecipe;

     public function __construct()
     {
         $this->vitamins = new ArrayCollection();
         $this->ingredientOfRecipe = new ArrayCollection();
     }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Ingredient
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Ingredient
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set carbohydrateG
     *
     * @param float $carbohydrateG
     * @return Ingredient
     */
    public function setCarbohydrateG($carbohydrateG)
    {
        $this->carbohydrateG = $carbohydrateG;

        return $this;
    }

    /**
     * Get carbohydrateG
     *
     * @return float 
     */
    public function getCarbohydrateG()
    {
        return $this->carbohydrateG;
    }

    /**
     * Set proteinG
     *
     * @param float $proteinG
     * @return Ingredient
     */
    public function setProteinG($proteinG)
    {
        $this->proteinG = $proteinG;

        return $this;
    }

    /**
     * Get proteinG
     *
     * @return float 
     */
    public function getProteinG()
    {
        return $this->proteinG;
    }

    /**
     * Set saturatedFatG
     *
     * @param float $saturatedFatG
     * @return Ingredient
     */
    public function setSaturatedFatG($saturatedFatG)
    {
        $this->saturatedFatG = $saturatedFatG;

        return $this;
    }

    /**
     * Get saturatedFatG
     *
     * @return float 
     */
    public function getSaturatedFatG()
    {
        return $this->saturatedFatG;
    }

    /**
     * Set polyUnsaturatedFatG
     *
     * @param float $polyUnsaturatedFatG
     * @return Ingredient
     */
    public function setPolyUnsaturatedFatG($polyUnsaturatedFatG)
    {
        $this->polyUnsaturatedFatG = $polyUnsaturatedFatG;

        return $this;
    }

    /**
     * Get polyUnsaturatedFatG
     *
     * @return float 
     */
    public function getPolyUnsaturatedFatG()
    {
        return $this->polyUnsaturatedFatG;
    }

    /**
     * Set monoUnsaturatedFatG
     *
     * @param float $monoUnsaturatedFatG
     * @return Ingredient
     */
    public function setMonoUnsaturatedFatG($monoUnsaturatedFatG)
    {
        $this->monoUnsaturatedFatG = $monoUnsaturatedFatG;

        return $this;
    }

    /**
     * Get monoUnsaturatedFatG
     *
     * @return float 
     */
    public function getMonoUnsaturatedFatG()
    {
        return $this->monoUnsaturatedFatG;
    }

    /**
     * Set transFatG
     *
     * @param float $transFatG
     * @return Ingredient
     */
    public function setTransFatG($transFatG)
    {
        $this->transFatG = $transFatG;

        return $this;
    }

    /**
     * Get transFatG
     *
     * @return float 
     */
    public function getTransFatG()
    {
        return $this->transFatG;
    }

    /**
     * Set kcalG
     *
     * @param float $kcalG
     * @return Ingredient
     */
    public function setKcalG($kcalG)
    {
        $this->kcalG = $kcalG;

        return $this;
    }

    /**
     * Get kcalG
     *
     * @return float 
     */
    public function getKcalG()
    {
        return $this->kcalG;
    }

    /**
     * Set cholesterolG
     *
     * @param float $cholesterolG
     * @return Ingredient
     */
    public function setCholesterolG($cholesterolG)
    {
        $this->cholesterolG = $cholesterolG;

        return $this;
    }

    /**
     * Get cholesterolG
     *
     * @return float 
     */
    public function getCholesterolG()
    {
        return $this->cholesterolG;
    }

    /**
     * Set potassiumG
     *
     * @param float $potassiumG
     * @return Ingredient
     */
    public function setPotassiumG($potassiumG)
    {
        $this->potassiumG = $potassiumG;

        return $this;
    }

    /**
     * Get potassiumG
     *
     * @return float 
     */
    public function getPotassiumG()
    {
        return $this->potassiumG;
    }

    /**
     * Set sodiumG
     *
     * @param float $sodiumG
     * @return Ingredient
     */
    public function setSodiumG($sodiumG)
    {
        $this->sodiumG = $sodiumG;

        return $this;
    }

    /**
     * Get sodiumG
     *
     * @return float 
     */
    public function getSodiumG()
    {
        return $this->sodiumG;
    }

    /**
     * Set calciumG
     *
     * @param float $calciumG
     * @return Ingredient
     */
    public function setCalciumG($calciumG)
    {
        $this->calciumG = $calciumG;

        return $this;
    }

    /**
     * Get calciumG
     *
     * @return float 
     */
    public function getCalciumG()
    {
        return $this->calciumG;
    }

    /**
     * Set ironG
     *
     * @param float $ironG
     * @return Ingredient
     */
    public function setIronG($ironG)
    {
        $this->ironG = $ironG;

        return $this;
    }

    /**
     * Get ironG
     *
     * @return float 
     */
    public function getIronG()
    {
        return $this->ironG;
    }

    /**
     * Set magnesiumG
     *
     * @param float $magnesiumG
     * @return Ingredient
     */
    public function setMagnesiumG($magnesiumG)
    {
        $this->magnesiumG = $magnesiumG;

        return $this;
    }

    /**
     * Get magnesiumG
     *
     * @return float 
     */
    public function getMagnesiumG()
    {
        return $this->magnesiumG;
    }

    /**
     * Add vitamins
     *
     * @param \RecipeBook\CoreBundle\Entity\Vitamin $vitamins
     * @return Ingredient
     */
    public function addVitamin(\RecipeBook\CoreBundle\Entity\Vitamin $vitamins)
    {
        $this->vitamins[] = $vitamins;

        return $this;
    }

    /**
     * Remove vitamins
     *
     * @param \RecipeBook\CoreBundle\Entity\Vitamin $vitamins
     */
    public function removeVitamin(\RecipeBook\CoreBundle\Entity\Vitamin $vitamins)
    {
        $this->vitamins->removeElement($vitamins);
    }

    /**
     * Get vitamins
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVitamins()
    {
        return $this->vitamins;
    }

    /**
     * Set ingredientType
     *
     * @param \RecipeBook\CoreBundle\Entity\IngredientTypeEntity $ingredientType
     * @return Ingredient
     */
    public function setIngredientType(IngredientTypeEntity $ingredientType = null)
    {
        $this->ingredientType = $ingredientType;

        return $this;
    }

    /**
     * Get ingredientType
     *
     * @return \RecipeBook\CoreBundle\Entity\IngredientTypeEntity
     */
    public function getIngredientType()
    {
        return $this->ingredientType;
    }

    /**
     * Add recipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $recipes
     * @return Ingredient
     */
    public function addRecipe(\RecipeBook\CoreBundle\Entity\Recipe $recipes)
    {
        $this->recipes[] = $recipes;

        return $this;
    }

    /**
     * Remove recipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $recipes
     */
    public function removeRecipe(\RecipeBook\CoreBundle\Entity\Recipe $recipes)
    {
        $this->recipes->removeElement($recipes);
    }

    /**
     * Get recipes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecipes()
    {
        return $this->recipes;
    }

    /**
     * Set fatG
     *
     * @param float $fatG
     * @return Ingredient
     */
    public function setFatG($fatG)
    {
        $this->fatG = $fatG;

        return $this;
    }

    /**
     * Get fatG
     *
     * @return float 
     */
    public function getFatG()
    {
        return $this->fatG;
    }

    /**
     * Add ingredientOfRecipe
     *
     * @param \RecipeBook\CoreBundle\Entity\RecipeIngredient $ingredientOfRecipe
     * @return Ingredient
     */
    public function addIngredientOfRecipe(\RecipeBook\CoreBundle\Entity\RecipeIngredient $ingredientOfRecipe)
    {
        $this->ingredientOfRecipe[] = $ingredientOfRecipe;

        return $this;
    }

    /**
     * Remove ingredientOfRecipe
     *
     * @param \RecipeBook\CoreBundle\Entity\RecipeIngredient $ingredientOfRecipe
     */
    public function removeIngredientOfRecipe(\RecipeBook\CoreBundle\Entity\RecipeIngredient $ingredientOfRecipe)
    {
        $this->ingredientOfRecipe->removeElement($ingredientOfRecipe);
    }

    /**
     * Get ingredientOfRecipe
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIngredientOfRecipe()
    {
        return $this->ingredientOfRecipe;
    }
}

<?php

namespace RecipeBook\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CountryRegion
 *
 * @ORM\Table(name="country_region")
 * @ORM\Entity
 */
class CountryRegion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="countryRegions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var Region
     *
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param \RecipeBook\CoreBundle\Entity\Country $country
     * @return CountryRegion
     */
    public function setCountry(\RecipeBook\CoreBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \RecipeBook\CoreBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param \RecipeBook\CoreBundle\Entity\Region $region
     * @return CountryRegion
     */
    public function setRegion(\RecipeBook\CoreBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \RecipeBook\CoreBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }
}

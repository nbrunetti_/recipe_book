<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 16/07/2016
 * Time: 03:06
 */

namespace RecipeBook\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Table(name="recipe")
 * @ORM\Entity(repositoryClass="RecipeBook\CoreBundle\Repository\RecipeRepository")
 */
class Recipe
{

    const RECIPE_COST_LOW       = 1;
    const RECIPE_COST_MEDIUM    = 2;
    const RECIPE_COST_HIGH      = 3;

    const RECIPE_DIFFICULTY_LOW         = 1;
    const RECIPE_DIFFICULTY_MEDIUM      = 2;
    const RECIPE_DIFFICULTY_HIGH        = 3;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $cost;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    protected $workingTime;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $difficulty;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $nPersonDose;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $recipeProcedure;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $advice;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $veg;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    protected $judgement;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $glutenFree;

    /**
     * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\RecipeCategory", inversedBy="recipes")
     * @ORM\JoinColumn(name="id_category", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\RecipeDish", inversedBy="recipes")
     * @ORM\JoinColumn(name="id_dish", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $dish;

    /**
     * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\RecipeFestivity", inversedBy="recipes")
     * @ORM\JoinColumn(name="id_festivity", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $festivity;

    /**
     * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\Country", inversedBy="recipes")
     * @ORM\JoinColumn(name="id_country", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\RecipeTypeEntity", inversedBy="recipes")
     * @ORM\JoinColumn(name="id_recipe_type", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $type;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $insertedAt;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="RecipeBook\CoreBundle\Entity\Review", mappedBy="recipes")
     */
    protected $reviews;

    /**
     * @ORM\ManyToOne(targetEntity="RecipeBook\CoreBundle\Entity\User", inversedBy="recipes")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="RecipeBook\CoreBundle\Entity\Tool", inversedBy="recipes")
     * @ORM\JoinTable(name="tools_of_recipes")
     */
    protected $tools;

    /**
     * @ORM\OneToMany(targetEntity="RecipeBook\CoreBundle\Entity\RecipeIngredient", mappedBy="recipe", cascade={"all"})
     */
    protected $recipeIngredients;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="RecipeBook\CoreBundle\Entity\Recipe", mappedBy="composedOfRecipes")
     */
    protected $partOfRecipes;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="RecipeBook\CoreBundle\Entity\Recipe", inversedBy="partOfRecipes")
     * @ORM\JoinTable(name="recipe_of_recipe",
     *     joinColumns={@ORM\JoinColumn(name="id_recipe", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="id_recipe_of_recipe", referencedColumnName="id")})
     */
    protected $composedOfRecipes;

    protected $ingredients;

    public function __construct()
    {
        $this->reviews = new ArrayCollection();
        $this->tools = new ArrayCollection();
        $this->recipeIngredients = new ArrayCollection();
        $this->partOfRecipes = new ArrayCollection();
        $this->composedOfRecipes = new ArrayCollection();
        $this->insertedAt = new \DateTime('now');
        $this->ingredients = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Recipe
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Recipe
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cost
     *
     * @param int $cost
     * @return Recipe
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set workingTime
     *
     * @param \DateTime $workingTime
     * @return Recipe
     */
    public function setWorkingTime($workingTime)
    {
        $this->workingTime = $workingTime;

        return $this;
    }

    /**
     * Get workingTime
     *
     * @return \DateTime 
     */
    public function getWorkingTime()
    {
        return $this->workingTime;
    }

    /**
     * Set difficulty
     *
     * @param integer $difficulty
     * @return Recipe
     */
    public function setDifficulty($difficulty)
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * Get difficulty
     *
     * @return integer 
     */
    public function getDifficulty()
    {
        return $this->difficulty;
    }

    /**
     * Set nPersonDose
     *
     * @param integer $nPersonDose
     * @return Recipe
     */
    public function setNPersonDose($nPersonDose)
    {
        $this->nPersonDose = $nPersonDose;

        return $this;
    }

    /**
     * Get nPersonDose
     *
     * @return integer 
     */
    public function getNPersonDose()
    {
        return $this->nPersonDose;
    }

    /**
     * Set advice
     *
     * @param string $advice
     * @return Recipe
     */
    public function setAdvice($advice)
    {
        $this->advice = $advice;

        return $this;
    }

    /**
     * Get advice
     *
     * @return string 
     */
    public function getAdvice()
    {
        return $this->advice;
    }

    /**
     * Set veg
     *
     * @param boolean $veg
     * @return Recipe
     */
    public function setVeg($veg)
    {
        $this->veg = $veg;

        return $this;
    }

    /**
     * Get veg
     *
     * @return boolean 
     */
    public function getVeg()
    {
        return $this->veg;
    }

    /**
     * Set glutenFree
     *
     * @param boolean $glutenFree
     * @return Recipe
     */
    public function setGlutenFree($glutenFree)
    {
        $this->glutenFree = $glutenFree;

        return $this;
    }

    /**
     * Get glutenFree
     *
     * @return boolean 
     */
    public function getGlutenFree()
    {
        return $this->glutenFree;
    }

    /**
     * Set insertedAt
     *
     * @param \DateTime $insertedAt
     * @return Recipe
     */
    public function setInsertedAt($insertedAt)
    {
        $this->insertedAt = $insertedAt;

        return $this;
    }

    /**
     * Get insertedAt
     *
     * @return \DateTime 
     */
    public function getInsertedAt()
    {
        return $this->insertedAt;
    }

    /**
     * Set category
     *
     * @param \RecipeBook\CoreBundle\Entity\RecipeCategory $category
     * @return Recipe
     */
    public function setCategory(\RecipeBook\CoreBundle\Entity\RecipeCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \RecipeBook\CoreBundle\Entity\RecipeCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set festivity
     *
     * @param \RecipeBook\CoreBundle\Entity\RecipeFestivity $festivity
     * @return Recipe
     */
    public function setFestivity(\RecipeBook\CoreBundle\Entity\RecipeFestivity $festivity = null)
    {
        $this->festivity = $festivity;

        return $this;
    }

    /**
     * Get festivity
     *
     * @return \RecipeBook\CoreBundle\Entity\RecipeFestivity 
     */
    public function getFestivity()
    {
        return $this->festivity;
    }

    /**
     * Set country
     *
     * @param \RecipeBook\CoreBundle\Entity\Country $country
     * @return Recipe
     */
    public function setCountry(\RecipeBook\CoreBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \RecipeBook\CoreBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set type
     *
     * @param \RecipeBook\CoreBundle\Entity\RecipeTypeEntity $type
     * @return Recipe
     */
    public function setType(\RecipeBook\CoreBundle\Entity\RecipeTypeEntity $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \RecipeBook\CoreBundle\Entity\RecipeTypeEntity 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add reviews
     *
     * @param \RecipeBook\CoreBundle\Entity\Review $reviews
     * @return Recipe
     */
    public function addReview(\RecipeBook\CoreBundle\Entity\Review $reviews)
    {
        $this->reviews[] = $reviews;

        return $this;
    }

    /**
     * Remove reviews
     *
     * @param \RecipeBook\CoreBundle\Entity\Review $reviews
     */
    public function removeReview(\RecipeBook\CoreBundle\Entity\Review $reviews)
    {
        $this->reviews->removeElement($reviews);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Set user
     *
     * @param \RecipeBook\CoreBundle\Entity\User $user
     * @return Recipe
     */
    public function setUser(\RecipeBook\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RecipeBook\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add tools
     *
     * @param \RecipeBook\CoreBundle\Entity\Tool $tools
     * @return Recipe
     */
    public function addTool(\RecipeBook\CoreBundle\Entity\Tool $tools)
    {
        $this->tools[] = $tools;

        return $this;
    }

    /**
     * Remove tools
     *
     * @param \RecipeBook\CoreBundle\Entity\Tool $tools
     */
    public function removeTool(\RecipeBook\CoreBundle\Entity\Tool $tools)
    {
        $this->tools->removeElement($tools);
    }

    /**
     * Get tools
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTools()
    {
        return $this->tools;
    }

    /**
     * Add recipeIngredients
     *
     * @param \RecipeBook\CoreBundle\Entity\RecipeIngredient $recipeIngredients
     * @return Recipe
     */
    public function addRecipeIngredient(\RecipeBook\CoreBundle\Entity\RecipeIngredient $recipeIngredients)
    {
        $this->recipeIngredients[] = $recipeIngredients;
        $recipeIngredients->setRecipe($this);

        return $this;
    }

    /** @var ArrayCollection $recipeIngredients */
    public function setRecipeIngredients($recipeIngredients){
        $this->recipeIngredients = $recipeIngredients;
        foreach ($recipeIngredients as $recipeIngredient){
            /** @var $recipeIngredient RecipeIngredient */
            $recipeIngredient->setRecipe($this);
        }
    }

    /**
     * Remove recipeIngredients
     *
     * @param \RecipeBook\CoreBundle\Entity\RecipeIngredient $recipeIngredients
     */
    public function removeRecipeIngredient(\RecipeBook\CoreBundle\Entity\RecipeIngredient $recipeIngredients)
    {
        $this->recipeIngredients->removeElement($recipeIngredients);
    }

    /**
     * Get recipeIngredients
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecipeIngredients()
    {
        return $this->recipeIngredients;
    }

    /**
     * Add partOfRecipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $partOfRecipes
     * @return Recipe
     */
    public function addPartOfRecipe(\RecipeBook\CoreBundle\Entity\Recipe $partOfRecipes)
    {
        $this->partOfRecipes[] = $partOfRecipes;

        return $this;
    }

    /**
     * Remove partOfRecipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $partOfRecipes
     */
    public function removePartOfRecipe(\RecipeBook\CoreBundle\Entity\Recipe $partOfRecipes)
    {
        $this->partOfRecipes->removeElement($partOfRecipes);
    }

    /**
     * Get partOfRecipes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPartOfRecipes()
    {
        return $this->partOfRecipes;
    }

    /**
     * Add composedOfRecipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $composedOfRecipes
     * @return Recipe
     */
    public function addComposedOfRecipe(\RecipeBook\CoreBundle\Entity\Recipe $composedOfRecipes)
    {
        $this->composedOfRecipes[] = $composedOfRecipes;

        return $this;
    }

    /**
     * Remove composedOfRecipes
     *
     * @param \RecipeBook\CoreBundle\Entity\Recipe $composedOfRecipes
     */
    public function removeComposedOfRecipe(\RecipeBook\CoreBundle\Entity\Recipe $composedOfRecipes)
    {
        $this->composedOfRecipes->removeElement($composedOfRecipes);
    }

    /**
     * Get composedOfRecipes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComposedOfRecipes()
    {
        return $this->composedOfRecipes;
    }

    /**
     * Set recipeProcedure
     *
     * @param string $recipeProcedure
     * @return Recipe
     */
    public function setRecipeProcedure($recipeProcedure)
    {
        $this->recipeProcedure = $recipeProcedure;

        return $this;
    }

    /**
     * Get recipeProcedure
     *
     * @return string 
     */
    public function getRecipeProcedure()
    {
        return $this->recipeProcedure;
    }

    public function getCostArrayName(){
        return array(
            self::RECIPE_COST_LOW       => 'basso',
            self::RECIPE_COST_HIGH      => 'alto',
            self::RECIPE_COST_MEDIUM    => 'medio'
        );
    }

    public function getDifficultyArrayName(){
        return array(
            self::RECIPE_DIFFICULTY_LOW       => 'bassa',
            self::RECIPE_DIFFICULTY_MEDIUM    => 'media',
            self::RECIPE_DIFFICULTY_HIGH      => 'alta'
        );
    }

    /**
     * Set dish
     *
     * @param \RecipeBook\CoreBundle\Entity\RecipeDish $dish
     * @return Recipe
     */
    public function setDish(\RecipeBook\CoreBundle\Entity\RecipeDish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \RecipeBook\CoreBundle\Entity\RecipeDish 
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * Set judgement
     *
     * @param float $judgement
     * @return Recipe
     */
    public function setJudgement($judgement)
    {
        $this->judgement = $judgement;

        return $this;
    }

    /**
     * Get judgement
     *
     * @return float 
     */
    public function getJudgement()
    {
        return $this->judgement;
    }
}

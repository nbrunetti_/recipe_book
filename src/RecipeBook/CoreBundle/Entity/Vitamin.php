<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 16/07/2016
 * Time: 15:39
 */

namespace RecipeBook\CoreBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vitamin")
 */
 class Vitamin
 {
     /**
      * @var integer
      * @ORM\Id
      * @ORM\Column(type="integer")
      * @ORM\GeneratedValue(strategy="AUTO")
      */
     protected $id;

     /**
      * @var string
      * @ORM\Column(type="string", nullable=true)
      */
     protected $name;

     /**
      * @var string
      * @ORM\Column(type="string", nullable=true)
      */
     protected $description;

     /**
      * @ORM\ManyToMany(targetEntity="RecipeBook\CoreBundle\Entity\Ingredient", mappedBy="vitamins")
      */
     protected $ingredients;

     public function __construct()
     {
         $this->ingredients = new ArrayCollection();
     }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Vitamin
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Vitamin
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add ingredients
     *
     * @param \RecipeBook\CoreBundle\Entity\Ingredient $ingredients
     * @return Vitamin
     */
    public function addIngredient(\RecipeBook\CoreBundle\Entity\Ingredient $ingredients)
    {
        $this->ingredients[] = $ingredients;

        return $this;
    }

    /**
     * Remove ingredients
     *
     * @param \RecipeBook\CoreBundle\Entity\Ingredient $ingredients
     */
    public function removeIngredient(\RecipeBook\CoreBundle\Entity\Ingredient $ingredients)
    {
        $this->ingredients->removeElement($ingredients);
    }

    /**
     * Get ingredients
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }
}

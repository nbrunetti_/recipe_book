<?php

namespace RecipeBook\CoreBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecipeIngredientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ingredient', EntityType::class, array(
                'required'      => false,
                'label'         => 'Ingrediente',
                'class'         => 'RecipeBook\CoreBundle\Entity\Ingredient',
                'choice_label'  => 'name',
                'attr'              => array('class' => 'form-control rb-select2-ingredient')
            ))
            ->add('quantity', TextType::class, array(
                'required'      => true,
                'label'         => 'Quantità:',
                'label_attr'    => array('class' => 'rb-form-label'),
                'attr'          => array('class' => 'form-control')
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'RecipeBook\CoreBundle\Entity\RecipeIngredient'
        ));

    }

    public function getName()
    {
        return 'recipe_book_core_bundle_recipe_ingredient_type';
    }
}

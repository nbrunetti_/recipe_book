<?php

namespace RecipeBook\CoreBundle\Form;

use RecipeBook\CoreBundle\Entity\Vitamin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VitaminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required'          => true,
                'label'             => 'Nome Vitamina:',
                'label_attr'        => array('class' => 'rb-form-label rb-vitamin-form-name')
            ))
            ->add('description', TextType::class, array(
                'required'          => false,
                'label'             => 'Descrizione:',
                'label_attr'        => array('class' => 'rb-form-label rb-vitamin-form-description')
            ))
            ->add('submit', SubmitType::class, array(
                'label'             => 'SALVA',
                'attr'              => array('class' => 'rb-form-label rb-submit-form-save')
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => Vitamin::class
        ));
    }

    public function getName()
    {
        return 'recipe_book_core_bundle_vitamin_type';
    }
}

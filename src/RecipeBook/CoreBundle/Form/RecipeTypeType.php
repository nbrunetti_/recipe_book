<?php

namespace RecipeBook\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecipeTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required'          => false,
                'label'             => 'Nome Tipo Ricetta:',
                'label_attr'        => array('class' => 'rb-form-label rb-recipe-type-form-name'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('description', TextareaType::class, array(
                'required'          => false,
                'label'             => 'Descrizione:',
                'label_attr'        => array('class' => 'rb-form-label rb-recipe-type-form-description'),
                'attr'              => array('class' => 'form-control rb-textarea')
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'recipe_book_core_bundle_recipe_type_type';
    }
}

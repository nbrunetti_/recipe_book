<?php

namespace RecipeBook\CoreBundle\Form;

use RecipeBook\CoreBundle\Entity\Recipe;
use RecipeBook\CoreBundle\Entity\RecipeCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecipeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ricetta = new Recipe();

        $builder
            ->add('name', TextType::class, array(
                'required'      => false,
                'label'         => 'Nome Ricetta:',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-name'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('description', TextareaType::class, array(
                'required'      => false,
                'label'         => 'Descrizione Ricetta:',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-description'),
                'attr'          => array('class' => 'form-control rb-textarea')
            ))
            ->add('recipeProcedure', 'ckeditor', array(
                'config'        => array('uiColor' => '#ffffff', 'jquery' => true),
                'required'      => false,
                'label'         => 'Procedimento Ricetta:',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-proceed'),
                'attr'          => array('class' => 'form-control rb-textarea')
            ))
            ->add('cost', ChoiceType::class, array(
                'required'      => false,
                'label'         => 'Costo',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-cost'),
                'choices'       => $ricetta->getCostArrayName(),
                'choice_attr'   => array('class' => 'rb-select2-cost'),
                'multiple'      => false,
                'attr'              => array('class' => 'form-control rb-select2-cost')
            ))
            ->add('workingTime', TimeType::class, array(
                'required'      => false,
                'label'         => 'Tempo Preparazione Ricetta:',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-workingtime'),
                'placeholder' => array(
                    'hour' => 'Ore', 'minute' => 'Minuti',
                ),
                'widget'        => 'choice',
                'input'         => 'datetime',
                'attr'              => array('class' => 'form-control rb-select2-working-time')
            ))
            ->add('difficulty', ChoiceType::class, array(
                'required'      => false,
                'label'         => 'Difficoltà',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-difficulty'),
                'choices'       => $ricetta->getDifficultyArrayName(),
                'choice_attr'   => array('class' => 'rb-select2-difficulty'),
                'multiple'      => false,
                'attr'              => array('class' => 'form-control rb-select2-difficulty')

            ))
            ->add('nPersonDose', IntegerType::class, array(
                'required'      => false,
                'label'         => 'Dosi per persone N°:',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-nperson'),
                'attr'              => array('class' => 'form-control rb-select2-nPersonDose')
            ))
            ->add('advice', 'ckeditor', array(
                'config'        => array('uiColor' => '#ffffff', 'jquery' => true),
                'required'      => false,
                'label'         => 'Consigli:',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-advice'),
                'attr'          => array('class' => 'form-control rb-textarea')
            ))
            ->add('veg', CheckboxType::class, array(
                'required'      => false,
                'label'         => 'Veg:',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-veg'),
                'attr'              => array('class' => 'form-control form-control-checkbox')
            ))
            ->add('gluten_free', CheckboxType::class, array(
                'required'      => false,
                'label'         => 'Gluten Free:',
                'label_attr'    => array('class' => 'rb-form-label rb-recipe-form-glutenfree'),
                'attr'              => array('class' => 'form-control form-control-checkbox')
            ))
            ->add('category', EntityType::class, array(
                'required'      => false,
                'label'         => 'Categoria',
                'class'         => 'RecipeBook\CoreBundle\Entity\RecipeCategory',
                'choice_label'  => 'name',
                'attr'              => array('class' => 'form-control rb-select2-category')
            ))
            ->add('dish', EntityType::class, array(
                'required'      => false,
                'label'         => 'Categoria',
                'class'         => 'RecipeBook\CoreBundle\Entity\RecipeCategory',
                'choice_label'  => 'name',
                'attr'              => array('class' => 'form-control rb-select2-dish')
            ))
            ->add('festivity', EntityType::class, array(
                'required'      => false,
                'label'         => 'Periodo/Festività:',
                'class'         => 'RecipeBook\CoreBundle\Entity\RecipeFestivity',
                'choice_label'  => 'name',
                'attr'              => array('class' => 'form-control rb-select2-festivity')
            ))
            ->add('country', EntityType::class, array(
                'required'      => false,
                'label'         => 'Nazione',
                'class'         => 'RecipeBook\CoreBundle\Entity\Country',
                'choice_label'  => 'name',
                'attr'          => array('class' => 'form-control rb-select2-country')
            ))
            ->add('tools', EntityType::class, array(
                'required'      => false,
                'label'         => 'Utensili',
                'class'         => 'RecipeBook\CoreBundle\Entity\Tool',
                'choice_label'  => 'name',
                'attr'          => array('class' => 'form-control rb-select2-tools'),
                'multiple'      => true
            ))
            ->add('recipeIngredients', 'collection', array(
                'label'        => 'Ingrediente di Ricetta:',
                'type'         => new RecipeIngredientType,
                'options'      => array(
                    'label' => ' '
                ),
                'allow_add'    => true,
                'allow_delete' => true,
                'prototype'    => true,
                'required'     => false,
                'by_reference' => false,
                'delete_empty' => true,
                'attr'         => array(
                    'class' => 'my-selector',
                ),
            ))
            ->add('composedOfRecipes', EntityType::class, array(
                'required'      => false,
                'label'         => 'Si compone di queste ricette:',
                'class'         => 'RecipeBook\CoreBundle\Entity\Recipe',
                'choice_label'  => 'name',
                'attr'          => array('class' => 'form-control rb-select2-recipe'),
                'multiple'      => true
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'RecipeBook\CoreBundle\Entity\Recipe'
        ));

    }

    public function getName()
    {
        return 'recipe_book_core_bundle_recipe_type';
    }
}

<?php

namespace RecipeBook\CoreBundle\Form;

use RecipeBook\CoreBundle\Entity\Ingredient;
use RecipeBook\CoreBundle\Entity\Vitamin;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IngredientType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required'          => true,
                'label'             => 'Nome Ingrediente:',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-name'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('description', TextareaType::class, array(
                'required'          => false,
                'label'             => 'Descrizione:',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-description'),
                'attr'              => array('class' => 'form-control rb-textarea'),
            ))
            ->add('ingredientType', EntityType::class, array(
                'required'      => false,
                'label'         => 'Tipo Ingrediente:',
                'class'         => 'RecipeBook\CoreBundle\Entity\IngredientTypeEntity',
                'choice_label'  => 'name',
                'attr'              => array('class' => 'form-control rb-select2-ingredient-type')
            ))
            ->add('carbohydrateG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Carboidrati(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-carbohydrates'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('proteinG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Proteine(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-protein'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('fatG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Grassi(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-fat'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('saturatedFatG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Grassi Saturi(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-fat'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('polyUnsaturatedFatG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Grassi Polinsaturi(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-fat'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('monoUnsaturatedFatG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Grassi Monoinsaturi(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-fat'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('transFatG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Grassi Transgenici(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-fat'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('cholesterolG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Colesterolo(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-water'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('potassiumG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Potassio(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-water'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('sodiumG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Sodio(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-water'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('calciumG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Calcio(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-water'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('ironG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Ferro(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-water'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('kcalG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Kcal(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-kcal'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('magnesiumG', NumberType::class, array(
                'required'          => false,
                'label'             => 'Magnesio(g):',
                'label_attr'        => array('class' => 'rb-form-label rb-ingredients-form-kcal'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('vitamins', EntityType::class, array(
                'class'             => 'RecipeBook\CoreBundle\Entity\Vitamin',
                'choice_label'      => 'name',
                'attr'              => array(
                    'class'    => 'rb-select2-vitamine'
                ),
                'multiple'          => 'true',
                'required'          => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'recipe_book_core_bundle_ingredient_type';
    }
}

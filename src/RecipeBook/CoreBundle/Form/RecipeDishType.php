<?php

namespace RecipeBook\CoreBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecipeDishType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required'          => false,
                'label'             => 'Nome singolare:',
                'label_attr'        => array('class' => 'rb-form-label rb-course-form-description'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('pluralName', TextType::class, array(
                'required'          => false,
                'label'             => 'Nome Plurale:',
                'label_attr'        => array('class' => 'rb-form-label rb-course-form-description'),
                'attr'              => array('class' => 'form-control')
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'recipe_book_core_bundle_portata_type';
    }
}

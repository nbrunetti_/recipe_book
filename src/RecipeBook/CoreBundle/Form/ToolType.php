<?php

namespace RecipeBook\CoreBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ToolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required'          => true,
                'label'             => 'Nome Utensile:',
                'label_attr'        => array('class' => 'rb-form-label rb-tool-form-name'),
                'attr'              => array('class' => 'form-control')
            ))
            ->add('description', TextareaType::class, array(
                'required'          => false,
                'label'             => 'Descrizione:',
                'label_attr'        => array('class' => 'rb-form-label rb-tool-form-description'),
                'attr'              => array('class' => 'form-control rb-textarea')
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'recipe_book_core_bundle_tool_type';
    }
}

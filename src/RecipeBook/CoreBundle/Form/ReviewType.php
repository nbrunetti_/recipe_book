<?php

namespace RecipeBook\CoreBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reviewsText', TextareaType::class, array(
                'required'          => true,
                'label'             => 'Testo Recensione:',
                'label_attr'        => array('class' => 'rb-form-label rb-review-form-text'),
                'attr'              => array('class' => 'form-control rb-textarea')
            ))
            ->add('judgement', IntegerType::class, array(
                'required'          => false,
                'label'             => 'Giudizio:',
                'label_attr'        => array('class' => 'rb-form-label rb-review-form-judgment'),
                'attr'              => array('class' => 'form-control ')
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'recipe_book_core_bundle_review_type';
    }
}

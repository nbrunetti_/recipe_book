<?php

namespace RecipeBook\CoreBundle\Twig\Extension;

use RecipeBook\CoreBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig_SimpleFilter;
use Twig_SimpleFunction;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Doctrine\ORM\EntityManager as EntityManager;

class CoreTwigExtension extends \Twig_Extension
{
    protected $controller;
    private $generator;
    /** @var ContainerInterface */
    private $container;
    /** @var  DoctrineRegistry $doctrine */
    private $doctrine;
    /** @var EntityManager  $em */
    private $em;

    public function __construct(UrlGeneratorInterface $generator,ContainerInterface $container,DoctrineRegistry $doctrine,EntityManager $em)
    {
        $this->generator = $generator;
        $this->container = $container;
        $this->doctrine = $doctrine;
        $this->em = $em;
//        $app_dir = $container->get('kernel')->getRootDir();
    }

    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {

        return array(

            new Twig_SimpleFunction('get_controller_name',array($this, 'getControllerName'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('get_action_name',array($this, 'getActionName'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('nav_list_controller',array($this, 'getSideNavButton'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('nav_list_controller_link',array($this, 'getSideNavButtonLink'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('print_if_controller',array($this, 'print_if_controller'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('active_if_controller',array($this, 'active_if_controller'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('active_if_controller_action',array($this, 'active_if_controller_action'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('print_if_bundle',array($this, 'print_if_bundle'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('role_name',array($this, 'role_name'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('check_link',array($this, 'check_link'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('var_export',array($this, 'var_export')),
            new Twig_SimpleFunction('get_env_name',array($this, 'get_env_name')),
            new Twig_SimpleFunction('check_user_role',array($this, 'check_user_role')),
            new Twig_SimpleFunction('get_file_size',array($this, 'get_file_size')),
            new Twig_SimpleFunction('file_exists',array($this, 'file_exists')),
            new Twig_SimpleFunction('is_user_grant_role',array($this, 'is_user_grant_role')),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('dateIta',array($this, 'dateIta')),
            new Twig_SimpleFilter('ucwords',array($this, 'ucWords')),
            new Twig_SimpleFilter('stripslashes',array($this, 'stripslashes')),
            new Twig_SimpleFilter('human_file_size',array($this, 'human_file_size')),
            new Twig_SimpleFilter('extension',array($this, 'get_extension')),
            new Twig_SimpleFilter('date_difference',array($this, 'date_difference')),
            new Twig_SimpleFilter('realpath',array($this, 'realpath')),
            new Twig_SimpleFilter('rawurlencode',array($this, 'rawurlencode')),
            new Twig_SimpleFilter('slugify',array($this, 'slugify'))
        );
    }

    /**
     * Get current controller name
     * @param bool $only_last
     * @return mixed
     */
    public function getControllerName($only_last = true)
    {
        $route_name = $this->container->get('request')->get('_route');
        $route_collection = $this->container->get('router')->getRouteCollection();
        $route_controller = $route_collection->get($route_name)->getDefault('_controller');
        list($controller, $action) = explode("::", $route_controller);

        if ($only_last) {
            $ar = explode('\\', $controller);
            $l = end($ar);
            if (!empty($l)) {
                return $l;
            }
        }

        return $controller;
    }

    /**
     * Get current action name
     */
    public function getActionName()
    {
        $route_name = $this->container->get('request')->get('_route');
        $route_collection = $this->container->get('router')->getRouteCollection();
        $route_controller = $route_collection->get($route_name)->getDefault('_controller');
        list($controller, $action) = explode("::", $route_controller);

        return $action;
    }

    /**
     * @param $label
     * @param $route
     * @param bool $is_action
     * @param array $params
     * @return string
     */
    public function getSideNavButtonLink($label, $route, $is_action = false, $params = array())
    {
        $url = $this->generator->generate($route, $params);

        $active = $this->checkActive($route,$is_action);

        return '<a ' . ($active ? ' class="active"' : '') . 'href="' . $url . '">' . $label . '</a>';
//        return '<a class="expandable '. ($active ? ' active' : '') .'" href="' . $url . '">' . $label . '</a>';
    }

    /**
     * @param $label
     * @param $route
     * @param bool $is_action
     * @param array $params
     * @return string
     */
    public function getSideNavButton($label, $route, $is_action = false, $params = array())
    {
        $url = $this->generator->generate($route, $params);

        $active = $this->checkActive($route,$is_action);

        return '<li><a ' . ($active ? ' class="active"' : '') . 'href="' . $url . '">' . $label . '</a></li>';
    }

    /**
     * @param $route
     * @param bool $check_action
     * @return bool
     */
    private function checkActive($route, $check_action = false)
    {
        $now_action = null;
        $bt_action = null;
        $now_route_name = $this->container->get('request')->get('_route');
        if ($route == $now_route_name) {
            return true;
        }

        $route_collection = $this->container->get('router')->getRouteCollection();

        $now_route_controller = $route_collection->get($now_route_name)->getDefault('_controller');
        $bt_route_controller = $route_collection->get($route)->getDefault('_controller');

        if (strpos($now_route_controller, "::") !== false) list($now_route_controller, $now_action) = explode("::", $now_route_controller);
        if (strpos($bt_route_controller, "::") !== false) list($bt_route_controller, $bt_action) = explode("::", $bt_route_controller);

        if ($check_action && $now_action != $bt_action) {
            return false;
        }

        return $now_route_controller == $bt_route_controller;
    }

    /**
     * @param $route
     * @param $html
     * @return string
     */
    public function print_if_controller($route, $html)
    {
        return $this->checkActive($route) ? $html : "";
    }

    /**
     * @param $route
     * @return string
     */
    public function active_if_controller($route)
    {
        return $this->checkActive($route) ? "active" : "";
    }

    /**
     * @param $route
     * @return string
     */
    public function active_if_controller_action($route)
    {
        return $this->checkActive($route,true) ? "active" : "";
    }

    /**
     * @param $bundle
     * @param $html
     * @return string
     */
    public function print_if_bundle($bundle, $html)
    {
        // UnifacileBackendBundle - non funziona.. :(
        //$bundle_name = $this->container -> get('request') ->attributes-> get('_template') -> get('bundle');
        $route_name = $this->container->get('request')->get('_route');
        $controller_path = $this->container->get('router')->getRouteCollection()->get($route_name)->getDefault('_controller');
        list($namespace, $bundle_name) = explode("\\", $controller_path);

        return $bundle == $bundle_name ? $html : "";
    }

    /**
     * @param $link
     * @return string
     */
    public function check_link($link)
    {
        if (substr($link, 0, 7) != 'http://') return "http://" . $link;
        return $link;
    }

    /**
     * @param $data
     * @param string $formato
     * @return string
     */
    public function dateIta($data, $formato = "%A %e %B %G")
    {
        if ($data instanceof \DateTime) $data = $data->getTimestamp();
        return ucwords(strftime($formato, $data));
    }

    /**
     * @param $stringa
     * @return mixed
     */
    public function ucWords($stringa)
    {
        $stringa = str_replace("'", "* ", $stringa);
        $ar = explode(' ', $stringa);
        $str = array();
        foreach ($ar as $s) array_push($str, (strlen($s) > 1 && strpos($s, "*") === false) ? ucfirst($s) : $s);
        $tmp = implode(' ', $str);
        return str_replace('* ', "'", $tmp);
    }

    /**
     * @param $stringa
     * @return string
     */
    public function stripslashes($stringa)
    {
        return stripslashes($stringa);
    }

    /**
     * @param $size
     * @return string
     */
    public function human_file_size($size)
    {
        $size = intval($size);
        $KB = 1024;
        $MB = 1024 * $KB;
        $GB = 1024 * $MB;

        if ($size / $KB < 0.9) {
            return strval($size) . " B";
        } else if ($size / $MB < 0.9) {
            return strval(floor($size / $KB * 10) / 10) . " KB";
        } else if ($size / $GB < 0.9) {
            return strval(floor($size / $MB * 10) / 10) . " MB";
        } else {
            return strval(floor($size / $GB * 10) / 10) . " GB";
        }
    }

    /**
     * @param $date
     * @return string
     */
    public function date_difference($date)
    {
        $now = new \DateTime("now");
        $d = $now->diff($date);
        if ($d->y == 0) {
            if ($d->m == 0) {
                if ($d->d == 0) {
                    if ($d->h == 0) {
                        if ($d->i == 0) {
                            return "Qualche istante fa";
                        } elseif ($d->i == 1) {
                            return "Un minuto fa";
                        } else {
                            return "$d->i minuti fa";
                        }
                    } elseif ($d->h == 1) {
                        return "Un'ora fa";
                    } else {
                        return "$d->h ore fa";
                    }
                } elseif ($d->d == 1) {
                    return "Un giorno fa";
                } else {
                    return "$d->d giorni fa";
                }
            } elseif ($d->m == 1) {
                return "Un mese fa";
            } else {
                return "$d->m mesi fa";
            }
        } elseif ($d->y == 1) {
            return "Un anno fa";
        } else {
            return "$d->y anni fa";
        }
    }

    /**
     * @param $mime
     * @return string
     */
    public function get_extension($mime)
    {
        if ($ext = explode("/", $mime)[1]) {
            return $ext;
        } else {
            return "Undefined";
        }
    }

    /**
     * @param $path
     * @return string
     */
    public function realpath($path)
    {
        return realpath($path);
    }

    /**
     * Wrapper della funzione php
     *
     * @param $misc
     * @return mixed
     */
    public function var_export($misc)
    {
        return var_export($misc);
    }

    /**
     * @param $path
     * @return bool
     */
    public function file_exists($path)
    {
        return file_exists($path);
    }

    /**
     * Ritorna il nome dell'ambiente (prod, dev)
     *
     * @return mixed
     */
    public function get_env_name()
    {
        return $this->container->getParameter('kernel.environment');
    }

    /**
     * Verifica che l'utente possegga la regola o le regole richieste
     *
     * @param User $user
     * @param $role
     * @return bool
     */
    public function check_user_role($user, $role)
    {
        if (is_array($role)) {
            foreach ($role as $r) {
                if (!$user->hasRole($r)) {
                    return false;
                }
            }
            return true;
        } else {
            return $user->hasRole($role);
        }
    }

    /**
     * Restituisce la dimensione del file
     *
     * @param $path
     * @return int|null
     */
    public function get_file_size($path)
    {
        if (!file_exists($path)) {
            return NULL;
        }
        return filesize($path);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'twig.extension.quadra.coretwig';
    }

    /**
     * @param User $user
     * @param $role
     * @return bool
     */
    public function is_user_grant_role(User $user, $role)
    {

        if ($user->hasRole($role)) {
            return true;
        }

        $system_roles = $this->container->getParameter('security.role_hierarchy.roles');
        $u_roles = [];
        foreach ($user->getRoles() as $r) {
            $u_roles[$r] = [];
        }


        foreach ($system_roles as $r => $subroles) {
            if (array_key_exists($r, $u_roles)) {
                $u_roles[$r] = $subroles;
            }
        }
        foreach ($u_roles as $r) {
            foreach ($r as $sub_user_roles) {

                if (
                    (is_array($sub_user_roles) && in_array($role, $sub_user_roles)) || (!is_array($sub_user_roles) && $role == $sub_user_roles)
                ) {
                    return true;
                }
            }

        }
        return false;
    }

    /**
     * @param $text
     * @return mixed|string
     */
    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        // trim
        $text = trim($text, '-');
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)){
            return 'n-a';
        }
        return $text;
    }

    /**
     * @param $str
     * @return string
     */
    public function rawurlencode($str)
    {
        return rawurlencode($str);
    }
}

<?php

namespace RecipeBook\AppBundle\Controller;

use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Entity\Recipe;
use RecipeBook\CoreBundle\Entity\RecipeDish;
use RecipeBook\CoreBundle\Entity\RecipeTypeEntity;
use RecipeBook\CoreBundle\Entity\Review;
use RecipeBook\CoreBundle\Form\RecipeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RicettaPublicController
 * @package RecipeBook\AppBundle\Controller
 *
 * @Route("/ricette")
 */
class RicettaPublicController extends BaseController
{

    /**
     * @return Response
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('');
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/ricetta/{id}", name="public_page_ricetta")
     */
    public function ricettaIndexAction($id, Request $request){

        $ricetta = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->find($id);

        $reviews = new Review();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\ReviewType', $reviews, array(
            'action'    => $this->generateUrl('public_page_ricetta', array('id' => $id)),
            'method'    => 'POST',
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $reviews->setRecipes($ricetta)->setUsers(($user));
                $em = $this->em();
                $em->persist($reviews);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Reviews aggiunta con successo");

                $ricetta->setJudgement($this->getRecipeAverageJudgement($ricetta));
                $em->persist($reviews);
                $em->flush();
                return $this->redirectToRoute('aggiungi_recensione', array('id' => $id));
            }
        }

        return $this->render('@RecipeBookApp/Ricetta/ricetta-public-page.html.twig', array(
            'ricetta'       => $ricetta,
            'form'          => $form->createView()
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/antipasti", name="antipasti_public_page")
     */
    public function antipastiIndexAction(Request $request){

        $recipeDish = $this->em()->getRepository('RecipeBookCoreBundle:RecipeDish')->find(
            array('id' => $this->getIdNameRecipeDishArray()['antipasto']));

        $query = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->getAllRecipeDishByParameters($recipeDish->getId());
        $antipasti = $query->getResult();

        $pagination  = $this->getPaginator()->paginate(
            $antipasti,
            $request->query->getInt('page', 1),
            10
        );


        return $this->render('@RecipeBookApp/Ricetta/antipasti-public-page.html.twig', array(
            'pagination'    => $pagination,
            'antipasti'     => $antipasti
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/primi-piatti", name="primi_piatti_public_page")
     */
    public function primiPiattiIndexAction(Request $request){
        $recipeDish = $this->em()->getRepository('RecipeBookCoreBundle:RecipeDish')->find(
            array('id' => $this->getIdNameRecipeDishArray()['primo']));

        $query = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->getAllRecipeDishByParameters($recipeDish->getId());
        $primiPiatti = $query->getResult();

        $pagination  = $this->getPaginator()->paginate(
            $primiPiatti,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookApp/Ricetta/primi-piatti-public-page.html.twig', array(
            'primi'     => $primiPiatti,
            'pagination'    => $pagination
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/secondi-piatti", name="secondi_piatti_public_page")
     */
    public function secondiPiattiIndexAction(Request $request){
        $recipeDish = $this->em()->getRepository('RecipeBookCoreBundle:RecipeDish')->find(
            array('id' => $this->getIdNameRecipeDishArray()['secondo']));

        $query = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->getAllRecipeDishByParameters($recipeDish->getId());
        $secondiPiatti = $query->getResult();

        $pagination  = $this->getPaginator()->paginate(
            $secondiPiatti,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookApp/Ricetta/secondi-piatti-public-page.html.twig', array(
            'secondi'       => $secondiPiatti,
            'pagination'    => $pagination
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/dessert", name="dessert_public_page")
     */
    public function dessertIndexAction(Request $request){
        $recipeDish = $this->em()->getRepository('RecipeBookCoreBundle:RecipeDish')->find(
            array('id' => $this->getIdNameRecipeDishArray()['dessert']));

        $query = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->getAllRecipeDishByParameters($recipeDish->getId());
        $dessert = $query->getResult();

        $pagination  = $this->getPaginator()->paginate(
            $dessert,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookApp/Ricetta/dessert-public-page.html.twig', array(
            'dessert'       => $dessert,
            'pagination'    => $pagination
        ));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/categoria/{id}", name="public_category_page")
     */
    public function categoryIndexAction($id, Request $request){
        $query = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->getAllRecipeByParameters('category', $id);
        $recipes = $query->getResult();

        $pagination  = $this->getPaginator()->paginate(
            $recipes,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookApp/Ricetta/category-public-page.html.twig', array(
            'recipes'       => $recipes,
            'pagination'    => $pagination,
            'categoryName' => $this->getIdNameCategoryArray()[$id]
        ));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/festivita/{id}", name="public_festivity_page")
     */
    public function festivityIndexAction($id, Request $request){
        $query = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->getAllRecipeByParameters('festivity', $id);
        $recipes = $query->getResult();

        $pagination  = $this->getPaginator()->paginate(
            $recipes,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookApp/Ricetta/festivity-public-page.html.twig', array(
            'recipes'       => $recipes,
            'pagination'    => $pagination,
            'festivityName' => $this->getIdNameFestivityArray()[$id]
        ));
    }

    private function getIdNameRecipeDishArray(){
        $dishes = $this->em()->getRepository('RecipeBookCoreBundle:RecipeDish')->findAll();

        $idNameRecipeDishArray = array();

        foreach ($dishes as $dish){
            $idNameRecipeDishArray[$dish->getName()] = $dish->getId();
        }

        return $idNameRecipeDishArray;
    }

    public function getIdNameFestivityArray(){
        $festivities = $this->em()->getRepository('RecipeBookCoreBundle:RecipeFestivity')->findAll();

        $idNameFestivityArray = array();

        foreach ($festivities as $festivity){
            $idNameFestivityArray[$festivity->getId()] = $festivity->getName();
        }

        return $idNameFestivityArray;
    }

    public function getIdNameCategoryArray(){
        $categories = $this->em()->getRepository('RecipeBookCoreBundle:RecipeCategory')->findAll();

        $idNameCategoryArray = array();

        foreach ($categories as $category){
            $idNameCategoryArray[$category->getId()] = $category->getName();
        }

        return $idNameCategoryArray;
    }


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/aggiungi-recensione/{id}", name="aggiungi_recensione"))
     */
    public function aggiungiRecensioneAction($id){
        return $this->redirectToRoute('public_page_ricetta', array('id' => $id));
    }

    /**
     * @param Recipe $recipe
     * @return float|int
     */
    private function getRecipeAverageJudgement(Recipe $recipe){
        $listOfReviews = $this->em()->getRepository('RecipeBookCoreBundle:Review')->findBy(array(
            'recipes' => $recipe
        ));

        $average = 0;
        $count = 0;

        foreach ($listOfReviews as $review){
            $average += $review->getJudgement();
            $count++;
        }

        $average = ceil($average/$count);
        return $average;
    }


}

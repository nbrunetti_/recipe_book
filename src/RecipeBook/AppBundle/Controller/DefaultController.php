<?php

namespace RecipeBook\AppBundle\Controller;

use RecipeBook\CoreBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * Class DefaultController
 * @package RecipeBook\AppBundle\Controller
 * @Route("/")
 */
class DefaultController extends BaseController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="public_home")
     */
    public function indexAction()
    {
        $categorie = $this->em()->getRepository('RecipeBookCoreBundle:RecipeCategory')->findAll();

        return $this->render('@RecipeBookApp/Default/index.html.twig', array(
            'categories' => $categorie
        ));
    }

}

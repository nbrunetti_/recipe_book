var RecipeBookMenuControllerClass = function ($) {
    var $this = this;

    var $tipicalityMenuSelect = $('#rb-select2-tipicality');

    $this.tipicalityMenuSelectListener = function () {

    };

    $this.prepareSelect2Elements = function () {
        $tipicalityMenuSelect.select2({
            placeholder: "Seleziona lo stato o la regione italiana",
            allowclear: true
        })
    };

    $this.init = function () {

    };

    $this.init();
};
(function ($) {
    RecipeBookMenuController = new RecipeBookMenuControllerClass($);
})(jQuery);
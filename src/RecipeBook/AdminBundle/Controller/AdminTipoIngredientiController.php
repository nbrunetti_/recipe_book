<?php

namespace RecipeBook\AdminBundle\Controller;

use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Form\IngredientTypeType;
use RecipeBook\CoreBundle\Entity\IngredientTypeEntity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminTipoIngredientiController
 * @package RecipeBook\AdminBundle\Controller
 * @Route("/admin/tipo-ingredienti")
 */
class AdminTipoIngredientiController extends BaseController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-tipo-ingrediente", name="admin_aggiungi_tipo_ingrediente")
     */
    public function formTipoIngredienteAction(Request $request){
        $tipoIngrediente = new IngredientTypeEntity();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\IngredientTypeType', $tipoIngrediente, array(
            'action'    => $this->generateUrl('admin_aggiungi_tipo_ingrediente'),
            'method'    => 'POST'
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->em();
                $em->persist($tipoIngrediente);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Tipo Ingrediente aggiunto con successo");

//                return $this->redirectToRoute('admin_tipo_ingredienti_list');
                return $this->redirectToRoute('admin_lista_tipo_ingredienti');
            }
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-tipo-ingrediente-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/lista-tipo-ingredienti", name="admin_lista_tipo_ingredienti")
     */
    public function tipoIngredienteListAction(Request $request){
        $listaTipoIngredienti = $this->em()->getRepository('RecipeBookCoreBundle:IngredientTypeEntity')->findAll();

        $pagination  = $this->getPaginator()->paginate(
            $listaTipoIngredienti,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-tipo-ingredienti-admin-page.twig', array(
            'pagination'    => $pagination,
            'listaEntita'   => $listaTipoIngredienti,
            'editAction'    => 'edit_tipo_ingrediente',
            'deleteAction'  => 'delete_tipo_ingrediente',
            'addAction'     => 'admin_aggiungi_tipo_ingrediente',
            'nomeEntita'    => 'tipo ingrediente'
        ));
    }

    /**
     * @Route("/modifica-tipo-ingrediente/{id}", name="edit_tipo_ingrediente")
     */
    public function editTipoIngredienteAction($id, Request $request){
        $tipoIngrediente = $this->em()->getRepository('RecipeBookCoreBundle:IngredientTypeEntity')->find($id);

        $form = $this->createForm(IngredientTypeType::class, $tipoIngrediente);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $this->em()->persist($tipoIngrediente);
            $this->em()->flush();
            return $this->redirectToRoute('admin_tipo_ingredienti_list');
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-tipo-ingrediente-admin-page.html.twig', array(
            'form'          => $form->createView(),
            'ingrediente'   => $tipoIngrediente
        ));
    }

    /**
     * @Route("/cancella-tipo-ingrediente/{id}", name="delete_tipo_ingrediente")
     * @param IngredientTypeEntity $tipoIngrediente
     * @return RedirectResponse
     */
    public function deleteTipoIngredienteAction(IngredientTypeEntity $tipoIngrediente){
        if($tipoIngrediente === null){
            return new RedirectResponse($this->generateUrl('admin_tipo_ingredienti_list'));
        }
        $this->em()->remove($tipoIngrediente);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_lista_tipo_ingredienti'));
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: quadra
 * Date: 09/01/17
 * Time: 12:39
 */

namespace RecipeBook\AdminBundle\Controller;


use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Entity\Vitamin;
use RecipeBook\CoreBundle\Form\VitaminType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminCountryController
 * @package RecipeBook\AdminBundle\Controller
 * @Route("/vitamine")
 */
class AdminVitamineController extends BaseController
{
    /**
     * @Route("/lista-vitamine", name="admin_vitamine_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function vitaminListAction(Request $request){
        $listaNazioni = $this->em()->getRepository('RecipeBookCoreBundle:Vitamin')->findAll();
        $pagination  = $this->getPaginator()->paginate(
            $listaNazioni,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-vitamine-admin-page.html.twig', array(
            'pagination'    => $pagination,
            'listaEntita'   => $listaNazioni,
            'editAction'    => 'edit_vitamine',
            'deleteAction'  => 'delete_vitamine',
            'addAction'     => 'admin_aggiungi_vitamine',
            'nomeEntita'    => 'vitamine'
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-vitamine", name="admin_aggiungi_vitamine")
     */
    public function formUtensileAction(Request $request){
        $vitamine = new Vitamin();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\VitaminType', $vitamine, array(
            'action'    => $this->generateUrl('admin_aggiungi_vitamine'),
            'method'    => 'POST'
        ));

        if ($request !== null){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->em();
                $em->persist($vitamine);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Utensile aggiunto con successo");
            }

            return $this->redirectToRoute('admin_vitamine_list');

        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-vitamine-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }
    

    /**
     * @Route("/modifica-vitamine/{id}", name="edit_vitamine")
     *
     */
    public function editIngredienteAction($id, Request $request){
        $vitamine = $this->em()->getRepository('RecipeBookCoreBundle:Vitamin')->find($id);

        $form = $this->createForm(VitaminType::class, $vitamine);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $this->em()->persist($vitamine);
            $this->em()->flush();
            return $this->redirectToRoute('admin_vitamine_list');
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-vitamine-admin-page.html.twig', array(
            'form'       => $form->createView(),
            'vitamine'   => $vitamine
        ));
    }

    /**
     * @Route("/cancella-vitamine/{id}", name="delete_vitamine")
     * @param Vitamin $vitamine
     * @return RedirectResponse
     */
    public function deleteUtensileAction(Vitamin $vitamine){
        if (null === $vitamine){
            return new RedirectResponse($this->generateUrl('admin_vitamine_list'));
        }
        $this->em()->remove($vitamine);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_vitamine_list'));
    }

}
<?php

namespace RecipeBook\AdminBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use RecipeBook\CoreBundle\Entity\Address;
use RecipeBook\CoreBundle\Entity\Addresses;
use Fuz\AppBundle\Form\AddressesType;
use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Entity\Recipe;
use RecipeBook\CoreBundle\Entity\RecipeCategory;
use RecipeBook\CoreBundle\Entity\RecipeDish;
use RecipeBook\CoreBundle\Entity\RecipeFestivity;
use RecipeBook\CoreBundle\Entity\RecipeIngredient;
use RecipeBook\CoreBundle\Entity\RecipeTypeEntity;
use RecipeBook\CoreBundle\Entity\User;
use RecipeBook\CoreBundle\Form\RecipeCategoryType;
use RecipeBook\CoreBundle\Form\RecipeDishType;
use RecipeBook\CoreBundle\Form\RecipeFestivityType;
use RecipeBook\CoreBundle\Form\RecipeType;
use RecipeBook\CoreBundle\Form\RecipeTypeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RicettaAdminController
 * @package RecipeBook\AdminBundle\Controller
 *
 * @Route("/admin/ricetta")
 */
class AdminRicettaController extends BaseController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/lista-ricette", name="admin_ricetta_list")
     */
    public function ricettaListAction(Request $request){
        $listaRicette = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->findAll();
        $pagination  = $this->getPaginator()->paginate(
            $listaRicette,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-ricette-admin-page.html.twig', array(
            'pagination'    => $pagination,
            'listaEntita'   => $listaRicette,
            'editAction'    => 'edit_ricetta',
            'deleteAction'  => 'delete_ricetta',
            'addAction'     => 'admin_aggiungi_ricetta',
            'nomeEntita'    => 'ricetta'
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-ricetta", name="admin_aggiungi_ricetta")
     */
    public function formRicettaAction(Request $request){
        $ricetta = new Recipe();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\RecipeType', $ricetta, array(
            'action'    => $this->generateUrl('admin_aggiungi_ricetta'),
            'method'    => 'POST'
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                /** @var User $user */
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $ricetta->setUser($user);
                $em = $this->em();
                $em->persist($ricetta);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Ricetta aggiunta con successo");

                return $this->redirectToRoute('admin_ricetta_list');
            }
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-ricetta-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/modifica-ricetta/{id}", name="edit_ricetta")
     *
     */
    public function editRicettaAction($id, Request $request){
        $ricetta = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->find($id);

        $form = $this->createForm(RecipeType::class, $ricetta);

        $originalIngredients = new ArrayCollection();


        foreach($ricetta->getRecipeIngredients() as $ingredient){
            $originalIngredients->add($ingredient);
        }

        $form->handleRequest($request);


        if($form->isSubmitted()){
            foreach ($originalIngredients as $ingredient){
                if(false === $ricetta->getRecipeIngredients()->contains($ingredient)){
                    $this->em()->remove($ingredient);
                }
            }

            $this->em()->persist($ricetta);
            $this->em()->flush();
            return $this->redirectToRoute('admin_ricetta_list');
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-ricetta-admin-page.html.twig', array(
            'form'          => $form->createView(),
            'ricetta'       => $ricetta
        ));
    }

    /**
     * @Route("/cancella-ricetta/{id}", name="delete_ricetta")
     * @param Recipe $ricetta
     * @return RedirectResponse
     */
    public function deleteRicettaeAction(Recipe $ricetta){
        if($ricetta === null){
            return new RedirectResponse($this->generateUrl('admin_ricetta_list'));
        }
        $this->em()->remove($ricetta);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_ricetta_list'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/lista-tipo-ricetta", name="admin_tipo_ricetta_list")
     */
    public function tipoRicettaListAction(Request $request){
        $listaTipoRicetta = $this->em()->getRepository('RecipeBookCoreBundle:RecipeTypeEntity')->findAll();
        $pagination  = $this->getPaginator()->paginate(
            $listaTipoRicetta,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-tipo-ricette-admin-page.html.twig', array(
            'pagination'    => $pagination,
            'listaEntita'  => $listaTipoRicetta,
            'editAction'    => 'edit_tipo_ricetta',
            'deleteAction'  => 'delete_tipo_ricetta',
            'addAction'     => 'admin_aggiungi_tipo_ricetta',
            'nomeEntita'    => 'tipo ricetta'
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-tipo-ricetta", name="admin_aggiungi_tipo_ricetta")
     */
    public function formTipoRicettaAction(Request $request){
        $tipoRicetta = new RecipeTypeEntity();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\RecipeTypeType', $tipoRicetta, array(
            'action'    => $this->generateUrl('admin_aggiungi_tipo_ricetta'),
            'method'    => 'POST'
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->em();
                $em->persist($tipoRicetta);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Tipo Ricetta aggiunto con successo");

                return $this->redirectToRoute('admin_tipo_ricetta_list');
            }
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-tipo-ricetta-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/modifica-tipo-ricetta/{id}", name="edit_tipo_ricetta")
     *
     */
    public function editTipoRicettaAction($id, Request $request){
        $tipoRicetta = $this->em()->getRepository('RecipeBookCoreBundle:RecipeTypeEntity')->find($id);

        $form = $this->createForm(RecipeTypeType::class, $tipoRicetta);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $this->em()->persist($tipoRicetta);
            $this->em()->flush();
            return $this->redirectToRoute('admin_tipo_ricetta_list');
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-tipo-ricetta-admin-page.html.twig', array(
            'form'          => $form->createView(),
            'tipoRicetta'   => $tipoRicetta
        ));
    }

    /**
     * @Route("/cancella-tipo-ricetta/{id}", name="delete_tipo_ricetta")
     * @param RecipeTypeEntity $tipoRicetta
     * @return RedirectResponse
     */
    public function deleteTipoRicettaeAction(RecipeTypeEntity $tipoRicetta){
        if($tipoRicetta === null){
            return new RedirectResponse($this->generateUrl('admin_tipo_ricetta_list'));
        }
        $this->em()->remove($tipoRicetta);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_tipo_ricetta_list'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/lista-categoria-ricetta", name="admin_categoria_ricetta_list")
     */
    public function categoriaRicettaListAction(Request $request){
        $listaCategoriaRicetta = $this->em()->getRepository('RecipeBookCoreBundle:RecipeCategory')->findAll();
        $pagination  = $this->getPaginator()->paginate(
            $listaCategoriaRicetta,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-categorie-admin-page.html.twig', array(
            'pagination'    => $pagination,
            'listaEntita'   => $listaCategoriaRicetta,
            'editAction'    => 'edit_categoria_ricetta',
            'deleteAction'  => 'delete_categoria_ricetta',
            'addAction'     => 'admin_aggiungi_categoria_ricetta',
            'nomeEntita'    => 'categoria ricetta'
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-categoria-ricetta", name="admin_aggiungi_categoria_ricetta")
     */
    public function formCategoriaRicettaAction(Request $request){
        $categoriaRicetta = new RecipeCategory();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\RecipeCategoryType', $categoriaRicetta, array(
            'action'    => $this->generateUrl('admin_aggiungi_categoria_ricetta'),
            'method'    => 'POST'
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->em();
                $em->persist($categoriaRicetta);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Categoria Ricetta aggiunto con successo");

                return $this->redirectToRoute('admin_categoria_ricetta_list');
            }
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-categoria-ricetta-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/modifica-categoria-ricetta/{id}", name="edit_categoria_ricetta")
     *
     */
    public function editCategoriaRicettaAction($id, Request $request){
        $categoriaRicetta = $this->em()->getRepository('RecipeBookCoreBundle:RecipeCategory')->find($id);

        $form = $this->createForm(RecipeCategoryType::class, $categoriaRicetta);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $this->em()->persist($categoriaRicetta);
            $this->em()->flush();
            return $this->redirectToRoute('admin_categoria_ricetta_list');
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-categoria-ricetta-admin-page.html.twig', array(
            'form'               => $form->createView(),
            'categoriaRicetta'   => $categoriaRicetta
        ));
    }

    /**
     * @Route("/cancella-categoria-ricetta/{id}", name="delete_categoria_ricetta")
     * @param RecipeCategory $categoriaRicetta
     * @return RedirectResponse
     */
    public function deleteCategoriaRicettaAction(RecipeCategory $categoriaRicetta){
        if($categoriaRicetta === null){
            return new RedirectResponse($this->generateUrl('admin_categoria_ricetta_list'));
        }
        $this->em()->remove($categoriaRicetta);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_categoria_ricetta_list'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/lista-portata-ricetta", name="admin_portata_ricetta_list")
     */
    public function portataRicettaListAction(Request $request){
        $listaPortataRicetta = $this->em()->getRepository('RecipeBookCoreBundle:RecipeDish')->findAll();
        $pagination  = $this->getPaginator()->paginate(
            $listaPortataRicetta,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-portate-admin-page.html.twig', array(
            'pagination'    => $pagination,
            'listaEntita'   => $listaPortataRicetta,
            'editAction'    => 'edit_portata_ricetta',
            'deleteAction'  => 'delete_portata_ricetta',
            'addAction'     => 'admin_aggiungi_portata_ricetta',
            'nomeEntita'    => 'portata ricetta'
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-portata-ricetta", name="admin_aggiungi_portata_ricetta")
     */
    public function formPortataRicettaAction(Request $request){
        $portataRicetta = new RecipeDish();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\RecipeDishType', $portataRicetta, array(
            'action'    => $this->generateUrl('admin_aggiungi_portata_ricetta'),
            'method'    => 'POST'
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->em();
                $em->persist($portataRicetta);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Portata Ricetta aggiunto con successo");

                return $this->redirectToRoute('admin_portata_ricetta_list');
            }
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-portata-ricetta-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/modifica-portata-ricetta/{id}", name="edit_portata_ricetta")
     *
     */
    public function editPortataRicettaAction($id, Request $request){
        $portataRicetta = $this->em()->getRepository('RecipeBookCoreBundle:RecipeDish')->find($id);

        $form = $this->createForm(RecipeDishType::class, $portataRicetta);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $this->em()->persist($portataRicetta);
            $this->em()->flush();
            return $this->redirectToRoute('admin_portata_ricetta_list');
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-portata-ricetta-admin-page.html.twig', array(
            'form'               => $form->createView(),
            'portataRicetta'   => $portataRicetta
        ));
    }

    /**
     * @Route("/cancella-portata-ricetta/{id}", name="delete_portata_ricetta")
     * @param RecipeDish $portataRicetta
     * @return RedirectResponse
     */
    public function deletePortataRicettaAction(RecipeDish $portataRicetta){
        if($portataRicetta === null){
            return new RedirectResponse($this->generateUrl('admin_portata_ricetta_list'));
        }
        $this->em()->remove($portataRicetta);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_portata_ricetta_list'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/lista-festivita-ricetta", name="admin_festivita_ricetta_list")
     */
    public function festivitaRicettaListAction(Request $request){
        $listaPortataRicetta = $this->em()->getRepository('RecipeBookCoreBundle:RecipeFestivity')->findAll();
        $pagination  = $this->getPaginator()->paginate(
            $listaPortataRicetta,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-festivita-admin-page.html.twig', array(
            'pagination'    => $pagination,
            'listaEntita'   => $listaPortataRicetta,
            'editAction'    => 'edit_festivita_ricetta',
            'deleteAction'  => 'delete_festivita_ricetta',
            'addAction'     => 'admin_aggiungi_festivita_ricetta',
            'nomeEntita'    => 'festivita ricetta'
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-festivita-ricetta", name="admin_aggiungi_festivita_ricetta")
     */
    public function formFestivitaRicettaAction(Request $request){
        $festivitaRicetta = new RecipeFestivity();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\RecipeFestivityType', $festivitaRicetta, array(
            'action'    => $this->generateUrl('admin_aggiungi_festivita_ricetta'),
            'method'    => 'POST'
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->em();
                $em->persist($festivitaRicetta);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Festivita Ricetta aggiunto con successo");

                return $this->redirectToRoute('admin_festivita_ricetta_list');
            }
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-festivita-ricetta-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/modifica-festivita-ricetta/{id}", name="edit_festivita_ricetta")
     *
     */
    public function editFestivitaRicettaAction($id, Request $request){
        $festivitaRicetta = $this->em()->getRepository('RecipeBookCoreBundle:RecipeFestivity')->find($id);

        $form = $this->createForm(RecipeFestivityType::class, $festivitaRicetta);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $this->em()->persist($festivitaRicetta);
            $this->em()->flush();
            return $this->redirectToRoute('admin_festivita_ricetta_list');
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-festivita-ricetta-admin-page.html.twig', array(
            'form'               => $form->createView(),
            'festivitaRicetta'   => $festivitaRicetta
        ));
    }

    /**
     * @Route("/cancella-festivita-ricetta/{id}", name="delete_festivita_ricetta")
     * @param RecipeFestivity $festivitaRicetta
     * @return RedirectResponse
     */
    public function deleteFestivitaRicettaAction(RecipeFestivity $festivitaRicetta){
        if($festivitaRicetta === null){
            return new RedirectResponse($this->generateUrl('admin_festivita_ricetta_list'));
        }
        $this->em()->remove($festivitaRicetta);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_festivita_ricetta_list'));
    }
}

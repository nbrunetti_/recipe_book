<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 08/01/2017
 * Time: 19:38
 */

namespace RecipeBook\AdminBundle\Controller;

use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Entity\Ingredient;
use RecipeBook\CoreBundle\Form\IngredientType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/admin/ingredienti")
 * Class AdminIngredientController
 * @package RecipeBook\AdminBundle\Controller
 */
class AdminIngredientController extends BaseController
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-ingrediente", name="admin_aggiungi_ingrediente")
     */
    public function formIngredienteAction(Request $request){
        $ingrediente = new Ingredient();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\IngredientType', $ingrediente, array(
            'action'    => $this->generateUrl('admin_aggiungi_ingrediente'),
            'method'    => 'POST',
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->em();
                $em->persist($ingrediente);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Ingrediente aggiunto con successo");
                return $this->redirectToRoute('admin_ingredienti_list');
            }
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-ingrediente-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/lista-ingredienti", name="admin_ingredienti_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ingredienteListAction(Request $request){
        $listaIngredienti = $this->em()->getRepository('RecipeBookCoreBundle:Ingredient')->findAll();

        $pagination  = $this->getPaginator()->paginate(
            $listaIngredienti,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-ingredienti-admin-page.twig', array(
            'pagination'    => $pagination,
            'listaEntita'  => $listaIngredienti,
            'editAction'    => 'edit_ingrediente',
            'deleteAction'  => 'delete_ingrediente',
            'addAction'     => 'admin_aggiungi_ingrediente',
            'nomeEntita'    => 'ingrediente'
        ));
    }

    /**
     * @Route("/modifica-ingrediente/{id}", name="edit_ingrediente")
     *
     */
    public function editIngredienteAction($id, Request $request){
            $ingrediente = $this->em()->getRepository('RecipeBookCoreBundle:Ingredient')->find($id);

            $form = $this->createForm(IngredientType::class, $ingrediente);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $this->em()->persist($ingrediente);
                $this->em()->flush();
                return $this->redirectToRoute('admin_ingredienti_list');
            }

            return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-ingrediente-admin-page.html.twig', array(
                'form'          => $form->createView(),
                'ingrediente'   => $ingrediente
            ));
    }

    /**
     * @Route("/cancella-ingrediente/{id}", name="delete_ingrediente")
     * @param Ingredient $ingrediente
     * @return RedirectResponse
     */
    public function deleteIngredienteAction(Ingredient $ingrediente){
        if($ingrediente === null){
            return new RedirectResponse($this->generateUrl('admin_ingredienti_list'));
        }
        $this->em()->remove($ingrediente);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_ingredienti_list'));
    }
}
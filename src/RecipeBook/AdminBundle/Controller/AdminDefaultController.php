<?php

namespace RecipeBook\AdminBundle\Controller;

use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Entity\Recipe;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package RecipeBook\AdminBundle\Controller
 * @Route("/admin")
 */
class AdminDefaultController extends BaseController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="admin_dashboard_page")
     */
    public function adminIndexAction()
    {
        $lastTenRecipes = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')
            ->findLastTenInsertedRecipe()
            ->getResult();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $listMyTenRecipes = null;

        if($user){
            $listMyTenRecipes = $this->em()->getRepository('RecipeBookCoreBundle:Recipe')->findBy(
                array('user' => $user->getId()),array('insertedAt'    => 'DESC'), 10);
        }

        return $this->render('@RecipeBookAdmin/Default/index-admin.html.twig', array(
            'last10Recipes'     => $lastTenRecipes,
            'myLast10Recipes'   => $listMyTenRecipes
        ));
    }


}
<?php

namespace RecipeBook\AdminBundle\Controller;

use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Entity\CategoriaRicetta;
use RecipeBook\CoreBundle\Entity\FestivitaRicetta;
use RecipeBook\CoreBundle\Entity\Ingrediente;
use RecipeBook\CoreBundle\Entity\PortataRicetta;
use RecipeBook\CoreBundle\Entity\TipologiaIngrediente;
use RecipeBook\CoreBundle\Entity\Utensile;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class SuperAdminController
 * @package RecipeBook\AdminBundle\Controller
 * @Route("/super")
 */
class SuperAdminController extends AdminDefaultController
{
//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/", name="super_admin_default_page")
//     */
//    public function indexAction()
//    {
//        return $this->render('@RecipeBookAdmin/Default/super-admin-default-page.html.twig', array());
//    }
//
//    /**
//     * @param Request $request
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/aggiungi-categoria", name="super_admin_aggiungi_categoria")
//     */
//    public function formCategoriaAction(Request $request){
//        $categoria = new CategoriaRicetta();
//
//        $form = $this->createForm('RecipeBook\CoreBundle\Form\CategoriaType', $categoria, array(
//            'action'    => $this->generateUrl('super_admin_aggiungi_categoria'),
//            'method'    => 'POST'
//        ));
//
//        if ($request !== null){
//            $form->handleRequest($request);
//            if($form->isSubmitted() && $form->isValid()){
//                $em = $this->em();
//                $em->persist($categoria);
//                $em->flush();
//                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Categoria Ricetta aggiunta con successo");
//            }
//
//        }
//
//        return $this->render('@RecipeBookAdmin/Default/Add/Admin/aggiungi-categoria-admin-page.html.twig', array(
//            'form'      => $form->createView()
//        ));
//    }
//
//    /**
//     * @param Request $request
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/aggiungi-nazione", name="super_admin_aggiungi_nazione")
//     */
//    public function formNazioneAction(Request $request){
//        $nazione = new NazioneRicetta();
//
//        $form = $this->createForm('RecipeBook\CoreBundle\Form\NazioneType', $nazione, array(
//            'action'    => $this->generateUrl('super_admin_aggiungi_nazione'),
//            'method'    => 'POST'
//        ));
//
//        if ($request !== null){
//            $form->handleRequest($request);
//            if($form->isSubmitted() && $form->isValid()){
//                $em = $this->em();
//                $em->persist($nazione);
//                $em->flush();
//                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Categoria Ricetta aggiunta con successo");
//            }
//
//        }
//
//        return $this->render('@RecipeBookAdmin/Default/Add/Admin/aggiungi-nazione-admin-page.html.twig', array(
//            'form'      => $form->createView()
//        ));
//    }
//
//    /**
//     * @param Request $request
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/aggiungi-portata", name="super_admin_aggiungi_portata")
//     */
//    public function formPortataAction(Request $request){
//        $portata = new PortataRicetta();
//
//        $form = $this->createForm('RecipeBook\CoreBundle\Form\PortataType', $portata, array(
//            'action'    => $this->generateUrl('super_admin_aggiungi_portata'),
//            'method'    => 'POST'
//        ));
//
//        if ($request !== null){
//            $form->handleRequest($request);
//            if($form->isSubmitted() && $form->isValid()){
//                $em = $this->em();
//                $em->persist($portata);
//                $em->flush();
//                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Categoria Ricetta aggiunta con successo");
//            }
//
//        }
//
//        return $this->render('@RecipeBookAdmin/Default/Add/Admin/aggiungi-portata-admin-page.html.twig', array(
//            'form'      => $form->createView()
//        ));
//    }
//
//    /**
//     * @param Request $request
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/aggiungi-festivita", name="super_admin_aggiungi_festivita")
//     */
//    public function formFestivitaAction(Request $request){
//        $festivita = new FestivitaRicetta();
//
//        $form = $this->createForm('RecipeBook\CoreBundle\Form\FestivitaType', $festivita, array(
//            'action'    => $this->generateUrl('super_admin_aggiungi_festivita'),
//            'method'    => 'POST'
//        ));
//
//        if ($request !== null){
//            $form->handleRequest($request);
//            if($form->isSubmitted() && $form->isValid()){
//                $em = $this->em();
//                $em->persist($festivita);
//                $em->flush();
//                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Categoria Ricetta aggiunta con successo");
//            }
//
//        }
//
//        return $this->render('@RecipeBookAdmin/Default/Add/Admin/aggiungi-festivita-admin-page.html.twig', array(
//            'form'      => $form->createView()
//        ));
//    }
}
<?php
/**
 * Created by PhpStorm.
 * User: quadra
 * Date: 09/01/17
 * Time: 12:39
 */

namespace RecipeBook\AdminBundle\Controller;


use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Entity\Country;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminCountryController
 * @package RecipeBook\AdminBundle\Controller
 * @Route("/nazioni")
 */
class AdminCountryController extends BaseController
{
    /**
     * @Route("/lista-nazioni", name="admin_nazioni_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function countryListAction(Request $request){
        $listaNazioni = $this->em()->getRepository('RecipeBookCoreBundle:Country')->findAll();
        $pagination  = $this->getPaginator()->paginate(
            $listaNazioni,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-nazioni-admin-page.html.twig', array(
            'pagination'    => $pagination,
            'entityName'    => 'nazioni',
            'listaEntita'  => $listaNazioni
        ));
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: quadra
 * Date: 09/01/17
 * Time: 11:34
 */

namespace RecipeBook\AdminBundle\Controller;

use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Entity\Tool;
use RecipeBook\CoreBundle\Form\ToolType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/admin/utensili")
 * Class AdminIngredientController
 * @package RecipeBook\AdminBundle\Controller
 */
class AdminToolController extends BaseController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-utensile", name="admin_aggiungi_utensile")
     */
    public function formUtensileAction(Request $request){
        $utensile = new Tool();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\ToolType', $utensile, array(
            'action'    => $this->generateUrl('admin_aggiungi_utensile'),
            'method'    => 'POST'
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->em();
                $em->persist($utensile);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Utensile aggiunto con successo");
            }

            return $this->redirectToRoute('admin_utensili_list');

        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-utensile-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/lista-utensili", name="admin_utensili_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function utensileListAction(Request $request){
        $listaUtensili = $this->em()->getRepository('RecipeBookCoreBundle:Tool')->findAll();
        $pagination  = $this->getPaginator()->paginate(
            $listaUtensili,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-utensili-admin-page.html.twig', array(
            'pagination'    => $pagination,
            'listaEntita'   => $listaUtensili,
            'editAction'    => 'edit_utensile',
            'deleteAction'  => 'delete_utensile',
            'addAction'     => 'admin_aggiungi_utensile',
            'nomeEntita'    => 'utensile'
        ));
    }

    /**
     * @Route("/modifica-utensile/{id}", name="edit_utensile")
     *
     */
    public function editIngredienteAction($id, Request $request){
        $utensile = $this->em()->getRepository('RecipeBookCoreBundle:Tool')->find($id);

        $form = $this->createForm(ToolType::class, $utensile);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $this->em()->persist($utensile);
            $this->em()->flush();
            return $this->redirectToRoute('admin_utensili_list');
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-utensile-admin-page.html.twig', array(
            'form'       => $form->createView(),
            'utensile'   => $utensile
        ));
    }

    /**
     * @Route("/cancella-utensile/{id}", name="delete_utensile")
     * @param Tool $utensile
     * @return RedirectResponse
     */
    public function deleteUtensileAction(Tool $utensile){
        if (null === $utensile){
            return new RedirectResponse($this->generateUrl('admin_utensili_list'));
        }
        $this->em()->remove($utensile);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_utensili_list'));
    }

}
<?php

namespace RecipeBook\AdminBundle\Controller;

use RecipeBook\CoreBundle\Controller\BaseController;
use RecipeBook\CoreBundle\Entity\Review;
use RecipeBook\CoreBundle\Form\ReviewType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminReviewController
 * @package RecipeBook\AdminBundle\Controller
 * @Route("/admin/recensioni")
 */
class AdminReviewController extends BaseController
{
    /**
     * @Route("/lista-recensioni", name="admin_recensioni_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recensioniListAction(Request $request){
        $listaRecensioni = $this->em()->getRepository('RecipeBookCoreBundle:Review')->findAll();
        $pagination  = $this->getPaginator()->paginate(
            $listaRecensioni,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@RecipeBookAdmin/Default/List/lista-recensioni-admin-page.html.twig', array(
            'pagination'    => $pagination,
            'listaEntita'   => $listaRecensioni,
            'editAction'    => 'edit_recensione',
            'deleteAction'  => 'delete_recensione',
            'addAction'     => 'admin_aggiungi_recensione',
            'nomeEntita'    => 'recensione'
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/aggiungi-recensione", name="admin_aggiungi_recensione")
     */
    public function formReviewsAction(Request $request){
        $recensione = new Review();

        $form = $this->createForm('RecipeBook\CoreBundle\Form\ReviewType', $recensione, array(
            'action'    => $this->generateUrl('admin_aggiungi_recensione'),
            'method'    => 'POST'
        ));

        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->em();
                $em->persist($recensione);
                $em->flush();
                $this->addFlash(BaseController::FLASH_MESSAGE_NOTICE, "Recensione aggiunta con successo");

                return $this->redirectToRoute('admin_recensioni_list');
            }
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-recensione-admin-page.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/modifica-recensione/{id}", name="edit_recensione")
     *
     */
    public function editRecensioneAction($id, Request $request){
        $recensione = $this->em()->getRepository('RecipeBookCoreBundle:Review')->find($id);

        $form = $this->createForm(ReviewType::class, $recensione);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $this->em()->persist($recensione);
            $this->em()->flush();
            return $this->redirectToRoute('admin_recensioni_list');
        }

        return $this->render('@RecipeBookAdmin/Default/Add/admin/aggiungi-recensione-admin-page.html.twig', array(
            'form'          => $form->createView(),
            'recensione'   => $recensione
        ));
    }

    /**
     * @Route("/cancella-recensione/{id}", name="delete_recensione")
     * @param Review $review
     * @return RedirectResponse
     */
    public function deleteRecensioneAction(Review $review){
        if($review === null){
            return new RedirectResponse($this->generateUrl('admin_recensioni_list'));
        }
        $this->em()->remove($review);
        $this->em()->flush();
        return $this->redirect($this->generateUrl('admin_recensioni_list'));
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 24/01/2017
 * Time: 03:36
 */

namespace RecipeBook\AdminBundle\Services;


class AdminMenuElementsServices
{

    const AGGIUNGI_TYPE = 'Aggiungi';
    const MODIFICA_TYPE = 'Aggiungi';
    const ELIMINA_TYPE = 'Aggiungi';
    const CERCA_TYPE = 'Aggiungi';

    public function getIngredientiMenu(){
        return array(
            self::AGGIUNGI_TYPE => array(
                  'iconClass'   => 'fa-plus',
                  'menuText'    => 'Aggiungi Ingrediente',
                  'liType'      => 'default',
              ),
            self::MODIFICA_TYPE => array(
                'iconClass'   => 'fa-pencil',
                'menuText'    => 'Modifica Ingrediente',
                'liType'      => 'default',
            ),
        );
    }

}
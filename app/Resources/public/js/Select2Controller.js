var Select2ControllerClass = function ($) {
    var $this = this;

    var $selectVitamine         = $('.rb-select2-vitamine');
    var $selectDifficulty       = $('.rb-select2-difficulty');
    var $selectCategory         = $('.rb-select2-category');
    var $selectDish             = $('.rb-select2-dish');
    var $selectFestivity        = $('.rb-select2-festivity');
    var $selectCountry          = $('.rb-select2-country');
    var $selectTools            = $('.rb-select2-tools');
    var $selectIngredient       = $('.rb-select2-ingredient');
    var $selectRecipe           = $('.rb-select2-recipe');
    var $selectIngredientType   = $('.rb-select2-ingredient-type');

    var $select2Search          = $('.rb-select2-search-filter');

    $this.prepareSelect2Items = function () {
        $selectVitamine.select2({
            placeholder: "Seleziona o digita la vitamina da aggiungere",
            allowclear: true
        });
        $selectDifficulty.select2({
            placeholder: "Seleziona o digita la difficoltà",
            allowclear: true
        });
        $selectCategory.select2({
            placeholder: "Seleziona o digita la categoria",
            allowclear: true
        });
        $selectDish.select2({
            placeholder: "Seleziona o digita la portata",
            allowclear: true
        });
        $selectFestivity.select2({
            placeholder: "Seleziona o digita il periodo/festività",
            allowclear: true
        });
        $selectCountry.select2({
            placeholder: "Seleziona o digita la nazione",
            allowclear: true
        });
        $selectTools.select2({
            placeholder: "Seleziona o digita l'utensile",
            allowclear: true
        });
        $selectIngredient.select2({
            placeholder: "Seleziona o digita l'ingrediente",
            allowclear: true
        });
        $selectRecipe.select2({
            placeholder: "Seleziona o digita la ricetta",
            allowclear: true
        });
        $selectIngredientType.select2({
            placeholder: "Seleziona o digita il tipo dell'ingrediente",
            allowclear: true
        });
        $select2Search.select2({
            placeholder: "Seleziona o digita l'elemento da cercare",
            allowclear: true
        });
    };

    $this.init = function () {
        $this.prepareSelect2Items();
    };

    $this.init();
};
(function ($) {
    Select2Controller = new Select2ControllerClass($);
})(jQuery);
var FilterHandlerClass = function ($) {
    var $this = this;

    var $select2Search  = $('.rb-select2-search-filter');
    var $searchButton   = $('.rb-btn-search');
    var action = null;

    $this.select2SearchListener = function () {
        $select2Search.change(function () {
            var idElements = $select2Search.val();
            $searchButton.attr('href', action+'/'+idElements);
        });
    };

    $this.init = function () {
        action = $select2Search.data('action');
        if(action){
            if(action.endsWith('/0')){
                action = action.replace('/0', '');
            }
            $this.select2SearchListener();
        }
    };

    $this.init();
};
(function ($) {
    FilterHandler = new FilterHandlerClass($);
})(jQuery);
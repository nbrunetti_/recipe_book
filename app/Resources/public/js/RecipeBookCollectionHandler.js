var RecipeBookCollectionHandlerClass = function ($) {
    var $this = this;

    var $recipeIngredients = $('.my-selector');

    $this.init = function () {
        $recipeIngredients.collection();
    };

    $this.init();
};
(function ($) {
    RecipeBookCollectionHandler = new RecipeBookCollectionHandlerClass($);
})(jQuery);
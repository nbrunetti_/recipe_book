#!/usr/bin/env bash
php app/console assetic:dump --env=dev --no-debug
php app/console assetic:dump --env=prod --no-debug
php app/console assets:install --symlink web